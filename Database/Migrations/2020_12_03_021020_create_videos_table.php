<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVideosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cms_video', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('title', 100)->comment('Title');
            $table->string('file_name', 100)->comment('File Name');
            $table->string('file_type', 30)->comment('File Type');
            $table->double('file_size', 12, 0)->comment('File Size');
            $table->boolean('is_active')->default(0)->comment('Status Aktif');
            $table->integer('order')->default(0)->comment('Order List');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cms_video');
    }
}
