@extends('cms::layouts.master')

@section('content')

    {!! Form::open([ 'route' => ['about.update', $data->id], 'method' => 'patch', 'class' => 'form-horizontal', 'files' => true, 'autocomplete' => 'off' ]) !!}
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title"></h3>
                        <div class="box-tools pull-right">
                            <button type="submit" class="btn btn-sm btn-info">Save</button>
                            <a class="btn btn-sm btn-danger" href="{!! route('about.index') !!}">
                                <i class="fa fa-times"></i>
                            </a>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="title" class="col-sm-2 control-label">Title <span class="required">*</span></label>
                                    <div class="col-sm-8">
                                        {!! Form::text('title', $data->title, [ 'class' => 'form-control', 'maxlength' => 100, 'required', 'autofocus']) !!}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="description" class="col-sm-2 control-label">Description <span class="required">*</span></label>
                                    <div class="col-sm-8">
                                        {!! Form::textarea('description', $data->description, [ 'class' => 'form-control', 'id' => 'description', 'maxlength' => 5000, 'required', 'autofocus']) !!}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="image" class="col-sm-2 control-label">Image <span class="required">*</span></label>
                                    <div class="col-sm-8">
                                        {!! Form::file('image', null, [ 'class' => 'form-control']) !!}
                                        <br>
                                        <img src="{!! asset('storage/about/' . $data->image) !!}" style="max-width: 300px; border-radius: 10px" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    {!! Form::close() !!}

@endsection

@section('scripts')
    <script>
        $(document).ready(function() {
            $("#description").wysihtml5();
        });
    </script>
@endsection