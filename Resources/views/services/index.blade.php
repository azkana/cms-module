@extends('cms::layouts.master')

@section('content')

    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title"></h3>
                    <div class="box-tools pull-right">
                        {{-- @can('service-create') --}}
                        <a class="btn btn-sm btn-primary" href="{!! route('service.create') !!}">
                            <i class="fa fa-plus"></i> Service Baru
                        </a>
                        {{-- @endcan --}}
                    </div>
                </div>
                <div class="box-body" style="min-height: 520px">
                    <table id="grid-service" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th style="width: 5%">No.</th>
                                <th>Image</th>
                                <th>Title</th>
                                <th>Description</th>
                                <th>Status</th>
                                <th>Created</th>
                                <th class="text-center" style="width: 5%">
                                    <i class="fa fa-navicon"></i>
                                </th>
                            </tr>
                        </thead>
                        <tbody class="small">
                            @foreach($data as $row)
                            <tr>
                                <td></td>
                                <td class="text-center" style="width: 50px">
                                    <img src="{!! asset('storage/services') . '/' . $row->image !!}" style="max-height: 40px" />
                                </td>
                                <td style="width: 200px">{!! $row->title !!}</td>
                                <td style="text-align: justify">{!! $row->description !!}</td>
                                <td style="width: 80px">
                                    @if($row->is_active == true)
                                        Active
                                    @elseif($row->is_active == false)
                                        Not Active
                                    @endif
                                </td>
                                <td style="width: 100px">{!! date('d-M-Y H:i', strtotime($row->created_at)) !!}</td>
                                <td class="text-center">
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-xs btn-default dropdown-toggle" data-toggle="dropdown">
                                            <span class="caret"></span>
                                            <span class="sr-only">Action</span>
                                        </button>
                                        <ul class="dropdown-menu dropdown-menu-right" role="menu">
                                            <li>
                                                <a href="{!! route('service.show', $row->id) !!}"><i class="fa fa-eye"></i>Detail</a>
                                            </li>
                                            <li>
                                                <a href="{!! route('service.edit', $row->id) !!}"><i class="fa fa-edit"></i>Edit</a>
                                            </li>
                                            <li>
                                                <a href="#" data-toggle="modal" data-target="#delete-{!!$row->id!!}"><i class="fa fa-trash"></i>Delete</a>
                                            </li>
                                        </ul>
                                    </div>
                                </td>
                            </tr>
                            <!-- Modal -->
                            <div class="modal fade modal-danger" id="delete-{!!$row->id!!}" tabindex="-1" role="dialog" aria-labelledby="delete">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        {!! Form::open([ 'route' => ['service.destroy', $row->id], 'method' => 'delete' ]) !!}
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                                    aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title" id="myModalLabel">Konfirmasi</h4>
                                        </div>
                                        <div class="modal-body">
                                            Apakah anda yakin akan menghapus service <b>{!! $row->title !!}</b>?
                                        </div>
                                        <div class="modal-footer">
                                            <button type="submit" class="btn btn-outline">Hapus</button>
                                        </div>
                                        {!! Form::close() !!}
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script>
        $(document).ready(function(){
            var t = $("#grid-service").DataTable({
                "columnDefs": [ 
                    {
                        "targets": 0,
                        "searchable": false,
                        "orderable": false,
                        "className": "dt-body-center",
                    },
                    {"targets": 1, "orderable": false},
                    {"targets": 3, "orderable": false},
                    {"targets": 6, "orderable": false},
                ],
                "order": [[5, 'desc']],
            });

            t.on('order.dt search.dt', function() {
                t.column(0, {search: 'applied', order: 'applied'}).nodes().each(function(cell, i) {
                    cell.innerHTML = i+1;
                });
            }).draw();
        });
    </script>
@endsection