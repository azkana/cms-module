@extends('cms::layouts.master')

@section('content')

    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title"></h3>
                    <div class="box-tools pull-right">
                        {{-- @can('client-create') --}}
                        <a class="btn btn-sm btn-primary" href="{!! route('client.create') !!}">
                            <i class="fa fa-plus"></i> Client Baru
                        </a>
                        {{-- @endcan --}}
                    </div>
                </div>
                <div class="box-body" style="min-height: 520px">
                    <table id="grid-client" class="table table-bordered table-striped" style="width: 100%">
                        <thead>
                            <tr>
                                <th class="text-center" style="width: 5%">No.</th>
                                <th class="text-center" style="width: 105px">Logo</th>
                                <th class="text-center">Nama Perusahaan</th>
                                <th class="text-center">Website</th>
                                <th class="text-center">Kategori</th>
                                <th class="text-center">Status</th>
                                <th class="text-center">Dibuat</th>
                                <th class="text-center" style="width: 5%">
                                    <i class="fa fa-navicon"></i>
                                </th>
                            </tr>
                        </thead>
                        <tbody class="small">
                            @forelse ($data as $row)
                            <tr>
                                <td class="text-center"></td>
                                <td class="text-center">
                                    <img src="{!! asset('storage/clients/') . '/' . $row->logo_img !!}" alt="" style="max-height: 40px; max-width:100px">
                                </td>
                                <td style="width: 200px">
                                    {!! $row->company_name !!}<br>{!! !empty($row->alias_name) ? '(' . $row->alias_name . ')' : null !!}
                                </td>
                                <td style="text-align: justify"><a href="http://{!! $row->website !!}" target="_blank">{!! $row->website !!}</a></td>
                                <td style="text-align: justify">{!! $row->category !!}</td>
                                <td style="width: 80px">
                                    @if($row->is_active == true)
                                        Active
                                    @elseif($row->is_active == false)
                                        Not Active
                                    @endif
                                </td>
                                <td style="width: 100px">{!! Carbon::parse($row->created_at)->formatLocalized('%e %B %Y %H:%M') !!}</td>
                                <td class="text-center">
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-xs btn-default dropdown-toggle" data-toggle="dropdown">
                                            <span class="caret"></span>
                                            <span class="sr-only">Action</span>
                                        </button>
                                        <ul class="dropdown-menu dropdown-menu-right" role="menu">
                                            <li>
                                                <a href="{!! route('client.edit', $row->id) !!}"><i class="fa fa-edit"></i>Edit</a>
                                            </li>
                                            <li>
                                                <a href="#" data-toggle="modal" data-target="#delete-{!!$row->id!!}"><i class="fa fa-trash"></i>Delete</a>
                                            </li>
                                        </ul>
                                    </div>
                                </td>
                            </tr>
                            <!-- Modal -->
                            <div class="modal fade modal-danger" id="delete-{!!$row->id!!}" tabindex="-1" role="dialog" aria-labelledby="delete">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        {!! Form::open([ 'route' => ['client.destroy', $row->id], 'method' => 'delete' ]) !!}
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                                    aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title" id="myModalLabel">Konfirmasi</h4>
                                        </div>
                                        <div class="modal-body">
                                            Apakah anda yakin akan menghapus Klien <b>{!! $row->company_name !!}</b>?
                                        </div>
                                        <div class="modal-footer">
                                            <button type="submit" class="btn btn-outline">Hapus</button>
                                        </div>
                                        {!! Form::close() !!}
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script>
        $(document).ready(function(){
            var t = $("#grid-client").DataTable({
                "columnDefs": [ 
                    {
                        "targets": 0,
                        "searchable": false,
                        "orderable": false,
                        "className": "dt-body-center",
                    },
                    {"targets": 1, "orderable": false},
                    {"targets": 7, "orderable": false},
                ],
                "order": [[6, 'desc']],
            });

            t.on('order.dt search.dt', function() {
                t.column(0, {search: 'applied', order: 'applied'}).nodes().each(function(cell, i) {
                    cell.innerHTML = i+1;
                });
            }).draw();
        });
    </script>
@endsection