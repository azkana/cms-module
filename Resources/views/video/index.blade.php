@extends('cms::layouts.master')

@section('content')

<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title"></h3>
                <div class="box-tools pull-right">
                    @can('cms-jobs-create')
                    <a class="btn btn-sm btn-primary" href="{!! route('video.create') !!}">
                        <i class="fa fa-plus"></i> Video Baru
                    </a>
                    @endcan
                </div>
            </div>
            <div class="box-body" style="min-height: 520px">
                <div class="table-responsive">
                    <table id="grid-jobs" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th style="width: 5%">No.</th>
                                <th>Title</th>
                                <th>File Name</th>
                                <th>File Type</th>
                                <th>File Size</th>
                                <th>Status</th>
                                <th>Order</th>
                                <th>Created</th>
                                <th class="text-center" style="width: 5%">
                                    <i class="fa fa-navicon"></i>
                                </th>
                            </tr>
                        </thead>
                        <tbody class="small">
                            @foreach($data as $row)
                            <tr>
                                <td></td>
                                <td>{!! $row->title !!}</td>
                                <td>{!! $row->file_name !!}</td>
                                <td>{!! $row->file_type !!}</td>
                                <td>{!! $row->file_size !!}</td>
                                <td>
                                    @if($row->is_active == true)
                                        Active
                                    @elseif($row->is_active == false)
                                        Not Active
                                    @endif
                                </td>
                                <td>{!! $row->order !!}</td>
                                <td>{!! $row->created_at !!}</td>
                                <td class="text-center">
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-xs btn-default dropdown-toggle" data-toggle="dropdown">
                                            <span class="caret"></span>
                                            <span class="sr-only">Action</span>
                                        </button>
                                        <ul class="dropdown-menu dropdown-menu-right" role="menu">
                                            {{-- <li> --}}
                                                {{-- <a href="{!! route('slider.show', $row->id) !!}"><i class="fa fa-eye"></i>Detail</a> --}}
                                            {{-- </li> --}}
                                            {{-- @can('cms-video-edit') --}}
                                            <li>
                                                <a href="{!! route('video.edit', $row->id) !!}"><i class="fa fa-edit"></i>Edit</a>
                                            </li>
                                            {{-- @endcan --}}
                                            {{-- @can('cms-video-delete') --}}
                                            <li>
                                                <a href="#" data-toggle="modal" data-target="#delete-{!!$row->id!!}"><i class="fa fa-trash"></i>Delete</a>
                                            </li>
                                            {{-- @endcan --}}
                                        </ul>
                                    </div>
                                </td>
                            </tr>
                            <!-- Modal -->
                            <div class="modal fade modal-danger" id="delete-{!!$row->id!!}" tabindex="-1" role="dialog" aria-labelledby="delete">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        {!! Form::open([ 'route' => ['video.destroy', $row->id], 'method' => 'delete' ]) !!}
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                                    aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title" id="myModalLabel">Konfirmasi</h4>
                                        </div>
                                        <div class="modal-body">
                                            Apakah anda yakin akan menghapus video <b>{!! $row->title !!}</b>?
                                        </div>
                                        <div class="modal-footer">
                                            <button type="submit" class="btn btn-outline">Hapus</button>
                                        </div>
                                        {!! Form::close() !!}
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')
    <script>
        $(document).ready(function(){
            var t = $("#grid-jobs").DataTable();
            $('.dataTables_scrollBody').css('height', '350px');
            t.on('order.dt search.dt', function() {
                t.column(0, {search: 'applied', order: 'applied'}).nodes().each(function(cell, i) {
                    cell.innerHTML = i+1;
                });
            }).draw();
        });
    </script>
@endsection