<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware(['web'])->group(function () {
    Route::name('cms.')->group(function() {

        Route::get('/', 'Frontend\HomeController@index')->name('home');
        Route::get('jobs', 'Frontend\HomeController@jobs')->name('job');
        Route::get('jobs/{slug}', 'Frontend\HomeController@jobsDetail')->name('job.detail');
        Route::get('clients', 'Frontend\HomeController@client')->name('client');
        Route::get('/info', 'Frontend\HomeController@info')->name('info');
        Route::get('/info/{id}', 'Frontend\HomeController@infoDetail')->name('info.detail');
        Route::get('contact', 'Frontend\HomeController@contactUs')->name('contact');
        Route::get('video/show/{id}', 'Frontend\HomeController@videoShow')->name('video');
        Route::get('test', 'Frontend\HomeController@test')->name('test');
        Route::get('test-result', 'Frontend\HomeController@testResult')->name('test.result');

    });
});

Route::middleware(['auth'])->group(function () {
    Route::prefix('manage')->group(function() {
        Route::prefix('cms')->group(function() {
            Route::name('job.')->group(function() {
                Route::get('jobs', 'JobController@index')->name('index')->middleware('permission:cms-jobs-list');
                Route::get('jobs/create', 'JobController@create')->name('create')->middleware('permission:cms-jobs-create');
                Route::post('jobs/create', 'JobController@store')->name('store')->middleware('permission:cms-jobs-create');
                Route::get('jobs/{id}/edit', 'JobController@edit')->name('edit')->middleware('permission:cms-jobs-edit');
                Route::patch('jobs/{id}', 'JobController@update')->name('update')->middleware('permission:cms-jobs-edit');
                Route::delete('jobs/{id}', 'JobController@destroy')->name('destroy')->middleware('permission:cms-jobs-delete');
            });
            Route::name('slider.')->group(function() {
                Route::get('sliders', 'SliderController@index')->name('index');
                Route::get('sliders/create', 'SliderController@create')->name('create');
                Route::post('sliders/create', 'SliderController@store')->name('store');
                Route::get('sliders/{id}/show', 'SliderController@show')->name('show');
                Route::get('sliders/{id}/edit', 'SliderController@edit')->name('edit');
                Route::patch('sliders/{id}', 'SliderController@update')->name('update');
                Route::delete('sliders/{id}', 'SliderController@destroy')->name('destroy');
            });
            Route::name('info.')->group(function() {
                Route::get('info', 'InfoController@index')->name('index');
                Route::get('info/create', 'InfoController@create')->name('create');
                Route::post('info/create', 'InfoController@store')->name('store');
                Route::get('info/{id}/show', 'InfoController@show')->name('show');
                Route::get('info/{id}/edit', 'InfoController@edit')->name('edit');
                Route::patch('info/{id}', 'InfoController@update')->name('update');
                Route::delete('info/{id}', 'InfoController@destroy')->name('destroy');
            });

            Route::name('video.')->group(function() {
                Route::get('videos', 'VideoController@index')->name('index');
                Route::get('videos/create', 'VideoController@create')->name('create');
                Route::post('videos/create', 'VideoController@store')->name('store');
                Route::get('videos/{id}/show', 'VideoController@show')->name('show');
                Route::get('videos/{id}/edit', 'VideoController@edit')->name('edit');
                Route::patch('videos/{id}', 'VideoController@update')->name('update');
                Route::delete('videos/{id}', 'VideoController@destroy')->name('destroy');
            });

            Route::name('about.')->group(function() {
                Route::get('about', 'AboutUsController@index')->name('index');
                Route::get('about/{id}/edit', 'AboutUsController@edit')->name('edit');
                Route::patch('about/{id}', 'AboutUsController@update')->name('update');
            });

            Route::name('service.')->group(function() {
                Route::get('service', 'ServiceController@index')->name('index');
                Route::get('service/create', 'ServiceController@create')->name('create');
                Route::post('service/create', 'ServiceController@store')->name('store');
                Route::get('service/{id}/show', 'ServiceController@show')->name('show');
                Route::get('service/{id}/edit', 'ServiceController@edit')->name('edit');
                Route::patch('service/{id}', 'ServiceController@update')->name('update');
                Route::delete('service/{id}', 'ServiceController@destroy')->name('destroy');
            });

            Route::name('client.')->group(function() {
                Route::get('client', 'ClientController@index')->name('index');
                Route::get('client/create', 'ClientController@create')->name('create');
                Route::post('client/create', 'ClientController@store')->name('store');
                Route::get('client/{id}/edit', 'ClientController@edit')->name('edit');
                Route::patch('client/{id}', 'ClientController@update')->name('update');
                Route::delete('client/{id}', 'ClientController@destroy')->name('destroy');
            });

            Route::name('contact.')->group(function() {
                Route::get('contact', 'ContactController@index')->name('index');
                Route::get('contact/create', 'ContactController@create')->name('create');
                Route::post('contact/create', 'ContactController@store')->name('store');
                Route::get('contact/{id}/show', 'ContactController@show')->name('show');
                Route::get('contact/{id}/edit', 'ContactController@edit')->name('edit');
                Route::patch('contact/{id}', 'ContactController@update')->name('update');
                Route::delete('contact/{id}', 'ContactController@destroy')->name('destroy');
            });

            Route::name('setting.')->group(function() {
                Route::get('settings', 'SettingsController@index')->name('index');
            });
        });
    });
});
