@extends('cms::layouts.master')

@section('content')

    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title"></h3>
                    <div class="box-tools pull-right">
                        {{-- @can('contact-create') --}}
                        <a class="btn btn-sm btn-primary" href="{!! route('contact.create') !!}">
                            <i class="fa fa-plus"></i> Contact Baru
                        </a>
                        {{-- @endcan --}}
                    </div>
                </div>
                <div class="box-body" style="min-height: 520px">
                    <table id="grid-service" class="table table-bordered table-striped" style="width: 100%">
                        <thead>
                            <tr>
                                <th class="text-center" style="width: 5%">No.</th>
                                <th class="text-center">Jenis Kantor</th>
                                <th class="text-center">Nama Kantor</th>
                                <th class="text-center">Nama Gedung</th>
                                <th class="text-center">Nama Jalan</th>
                                <th class="text-center">Wilayah</th>
                                <th class="text-center">No. Telpon</th>
                                <th class="text-center">Status</th>
                                <th class="text-center">Dibuat</th>
                                <th class="text-center" style="width: 5%">
                                    <i class="fa fa-navicon"></i>
                                </th>
                            </tr>
                        </thead>
                        <tbody class="small">
                            @foreach($data as $row)
                            <tr>
                                <td class="text-center">{!! $no++ !!}</td>
                                <td class="text-center">
                                    {!! $row->is_branch == 0 ? 'Kantor Pusat' : 'Kantor Cabang' !!}
                                </td>
                                <td style="width: 200px">{!! $row->company_name !!}</td>
                                <td style="text-align: justify">{!! $row->building_name !!}</td>
                                <td style="text-align: justify">{!! $row->street_name !!}</td>
                                <td style="text-align: justify">{!! $row->street_area !!}</td>
                                <td style="text-align: justify">{!! $row->phone !!}</td>
                                <td style="width: 80px">
                                    @if($row->is_active == true)
                                        Active
                                    @elseif($row->is_active == false)
                                        Not Active
                                    @endif
                                </td>
                                <td style="width: 100px">{!! Carbon::parse($row->created_at)->formatLocalized('%e %B %Y %H:%M') !!}</td>
                                <td class="text-center">
                                    {{-- <div class="btn-group">
                                        <button type="button" class="btn btn-xs btn-default dropdown-toggle" data-toggle="dropdown">
                                            <span class="caret"></span>
                                            <span class="sr-only">Action</span>
                                        </button>
                                        <ul class="dropdown-menu dropdown-menu-right" role="menu">
                                            <li>
                                                <a href="{!! route('contact.show', $row->id) !!}"><i class="fa fa-eye"></i>Detail</a>
                                            </li>
                                            <li>
                                                <a href="{!! route('contact.edit', $row->id) !!}"><i class="fa fa-edit"></i>Edit</a>
                                            </li>
                                            <li>
                                                <a href="#" data-toggle="modal" data-target="#delete-{!!$row->id!!}"><i class="fa fa-trash"></i>Delete</a>
                                            </li>
                                        </ul>
                                    </div> --}}
                                </td>
                            </tr>
                            <!-- Modal -->
                            <div class="modal fade modal-danger" id="delete-{!!$row->id!!}" tabindex="-1" role="dialog" aria-labelledby="delete">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        {!! Form::open([ 'route' => ['contact.destroy', $row->id], 'method' => 'delete' ]) !!}
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                                    aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title" id="myModalLabel">Konfirmasi</h4>
                                        </div>
                                        <div class="modal-body">
                                            Apakah anda yakin akan menghapus kontak <b>{!! $row->company_name !!}</b>?
                                        </div>
                                        <div class="modal-footer">
                                            <button type="submit" class="btn btn-outline">Hapus</button>
                                        </div>
                                        {!! Form::close() !!}
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection