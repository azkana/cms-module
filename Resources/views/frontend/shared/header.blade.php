<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">

        <div class="navbar-header topnav">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="{!! route('cms.home') !!}">
                <img alt="Logo" src="{!! asset('modules/cms/frontend/img/logo-dku.png') !!}" style="margin-top: -26px; max-width: 80px">
            </a>
        </div>
        <div class="navbar-header topnav pull-right" style="margin-left: 50px">
            <a href="{!! route('member.register.index') !!}" class="btn btn-primary" style="margin-top: 10px;" role="button">DAFTAR</a>
        </div>
        <div class="collapse navbar-collapse topnav" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="{!! route('cms.home') !!}">Beranda</a></li>
                <li><a href="{!! route('cms.job') !!}">Lowongan</a></li>
                {{-- <li><a href="{!! route('cms.client') !!}">Klien</a></li> --}}
                <li><a href="{!! route('cms.info') !!}">Info</a></li>
                <li><a href="{!! route('cms.test') !!}">Test</a></li>
                <li><a href="{!! route('cms.test.result') !!}">Hasil Test</a></li>
                <li><a href="{!! route('cms.contact') !!}">Kontak</a></li>
            </ul>
        </div>

    </div>
</nav>
