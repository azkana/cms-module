@extends('cms::frontend.layouts.app')

@section('content')

	<div class="jumbotron page-banner">
		<div class="container">
			<h2 style="padding-top: 30px"><i>Klien</i></h2>
		</div>
	</div>

	<div class="container" style="padding-top:10px">
		@if($data->count() > 0)
		<div class="row" style="margin-bottom: 20px">
			<div class="col-md-12">
				<div class="btn-group btn-group-xs" role="group" aria-label="...">
					<button type="button" class="btn btn-default btn-cat active" id="all" style="min-width: 100px">All</button>
					@foreach($category as $cat)
					<button type="button" class="btn btn-default btn-cat" id="{!! camel_case($cat->category) !!}" style="min-width: 100px">{!! $cat->category !!}</button>
					@endforeach
				  </div>
			</div>
		</div>
		@endif
		<div class="row">
			@foreach ($data as $row)
		  	<div class="col-md-4 client-card {!! camel_case($row->category) !!}">
			  	<div class="panel panel-default">
				  	<div class="panel-body">
						<div class="row">
							<div class="col-md-4 text-center">
								<img src="{!! asset('storage/clients/' . $row->logo_img) !!}" class="border border-primary" alt="" style="max-height: 40px; max-width: 100px">
							</div>
							<div class="col-md-8">
								<h5>
									<a href="{!! !empty($row->website) ? 'http://'.$row->website : '#' !!}" target="_blank">
										{!! $row->company_name !!}
									</a>
								</h5>
							</div>
						</div>
				  	</div>
			  	</div>
		  	</div>
			@endforeach
		</div>
		{{-- <div class="row">
			<div class="col-md-12">
				<nav aria-label="...">
					<ul class="pagination pagination-sm">
					  <li class="disabled"><a href="#" aria-label="Previous"><span aria-hidden="true">&laquo;</span></a></li>
					  <li class="active"><a href="#">1 <span class="sr-only">(current)</span></a></li>
					  <li class=""><a href="#">2 </a></li>
					  <li class=""><a href="#">&raquo; </a></li>
					</ul>
				  </nav>
			</div>
		</div> --}}
  	</div>

@endsection

@section('css')

	<style>
		.page-banner {
			background-image: linear-gradient(rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)), url('{!! asset('modules/cms/frontend/img/clients-banner.jpg') !!}');
			background-repeat: no-repeat;
			background-size: cover;
			background-position: center;
			color: #ffffff;
		}
	</style>

@endsection

@section('js')
<script type="text/javascript">
	document.addEventListener('DOMContentLoaded', (event) => {
		var buttons = document.getElementsByClassName('btn-cat');
		var clientCard = $('.client-card');
		var buttonsCount = buttons.length;
		for(let i = 0; i < buttonsCount; i++) {
			buttons[i].onclick = function(e) {
				$('.btn-cat').removeClass('active');
				this.className = 'btn btn-default btn-cat active';
				var card = document.getElementsByClassName(this.id);
				if(this.id === 'all') {
					clientCard.removeClass('hide');
				} else {
					$('.row').children('.client-card.' + this.id).removeClass('hide');
					$('.row').children('.client-card:not(.' + this.id + ')').addClass('hide');
				}
			}
		}
	});
</script>
@endsection