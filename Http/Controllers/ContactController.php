<?php

namespace Modules\CMS\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\CMS\Entities\Contact;

class ContactController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        setLocale(LC_TIME, 'id');
        $params['pageTitle']    = 'Contact';
        $params['pageSubTitle'] = 'Index';
        $params['no']           = 1;
        $params['data']         = Contact::all();
        return view('cms::contacts.index', $params);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $params['pageTitle']    = 'Contact';
        $params['pageSubTitle'] = 'Create';
        $params['mapZoomLevel'] = [
            1   => 1,
            5   => 5,
            10  => 10,
            15  => 15,
            20  => 20
        ];
        return view('cms::contacts.create', $params);
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        try {
            $data['company_name']   = trim($request->company_name);
            $data['building_name']  = trim($request->building_name);
            $data['street_name']    = trim($request->street_name);
            $data['street_area']    = trim($request->street_area);
            $data['phone']          = trim($request->phone);
            $data['fax']            = trim($request->fax);
            $data['email']          = trim($request->email);
            $data['is_branch']      = $request->is_branch == null ? "0" : "1";
            $data['has_map']        = $request->has_map == null ? "0" : "1";
            $data['latitude']       = trim($request->latitude);
            $data['longitude']      = trim($request->longitude);
            $data['zoom']           = $request->zoom;
            $data['is_active']      = $request->is_active == null ? "0" : "1";
            Contact::create($data);
            return redirect(route('contact.index'))
				->with('success', trans('message.success_save'));
        } catch(\Exception $e) {
            return redirect(route('contact.index'))
				->with('error', trans('message.error_save').$e);
        }
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('cms::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('cms::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
}
