@extends('cms::layouts.master')

@section('content')

<div class="row">
    <div class="col-md-12">

        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#setting_general" data-toggle="tab">General</a></li>
                <li><a href="#setting_maintenance" data-toggle="tab">Maintenance</a></li>
            </ul>
            <div class="tab-content" style="min-height: 520px">
                <div class="tab-pane active" id="setting_general">
                    {!! Form::open([ 'route' => 'general.store', 'class' => 'form-horizontal', 'autocomplete' => 'off', 'files' => true ]) !!}
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="cms_copyright" class="col-sm-5 control-label">Copyright</label>
                                <div class="col-sm-7">
                                    {!! Form::text('cms_copyright', getOptions('cms_copyright'), [ 'class' => 'form-control']) !!}
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="cms_year" class="col-sm-5 control-label">Year</label>
                                <div class="col-sm-5">
                                    {!! Form::text('cms_year', getOptions('cms_year'), [ 'class' => 'form-control']) !!}
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="cms_favicon" class="col-sm-5 control-label">Favicon</label>
                                <div class="col-sm-7">
                                    @if(!empty(getOptions('cms_favicon')))
                                    <img class="img-responsive" src="{!! asset('storage/img/' . getOptions('cms_favicon')) !!}" alt="Photo" style="max-height:100px" />
                                    @endif
                                    {!! Form::file('cms_favicon', null, [ 'class' => 'form-control']) !!}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6"></div>
                    </div>
                    {!! Form::close() !!}
                </div>
                <div class="tab-pane" id="setting_maintenance">
                    {!! Form::open([ 'route' => 'general.store', 'class' => 'form-horizontal', 'autocomplete' => 'off', 'files' => true ]) !!}
                    <div class="form-group">
                        <label for="cms_is_maintenance" class="col-sm-3 control-label">Maintenance ?</label>
                        <div class="col-sm-6">
                            {!! Form::text('cms_is_maintenance', getOptions('cms_is_maintenance'), [ 'class' => 'form-control']) !!}
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>

    </div>
</div>

@endsection