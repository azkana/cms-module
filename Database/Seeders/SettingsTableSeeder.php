<?php

namespace Modules\CMS\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Models\Configs\General;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $timeStamp      = date('Y-m-d H:i:s');

        $isMaintenance  = 
        [
            'option_name'   => 'cms_is_maintenance',
            'option_value'  => '0',
            'autoload'      => 'yes',
            'created_at'    => $timeStamp,
            'updated_at'    => $timeStamp
        ];

        if($this->checkDataIsExist('option_name') == 0)
        {
            General::create($isMaintenance);
            $this->command->info('Option cms_is_maintenance created successfully.');
        }

        
    }

    private function checkDataIsExist($option_name) 
    {
        $general = General::where('option_name', $option_name)->get();
        return $general->count();
    }
}
