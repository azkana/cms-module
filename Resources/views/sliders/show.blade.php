@extends('cms::layouts.master')

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Sliders</h3>
                    <div class="box-tools pull-right">
                        <a class="btn btn-sm btn-danger" href="{!! route('slider.index') !!}">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <table class="table table-bordered table-hover small">
                                <tr>
                                    <th width="30%">Image</th>
                                    <th>{!! $data->is_image == true ? 'Yes' : 'No' !!}</th>
                                </tr>
                                @if($data->is_image == true)
                                    @if(!empty($data->slider_img))
                                    <tr>
                                        <th></th>
                                        <td>
                                            <img src="{!! _get_file($data->slider_img, '/cms/sliders/', config('cms.disk')) !!}" style="max-height: 150px; border-radius: 10px" />
                                        </td>
                                    </tr>
                                    @endif
                                @endif
                                <tr>
                                    <th>Text</th>
                                    <th>{!! $data->is_text == true ? 'Yes' : 'No' !!}</th>
                                </tr>
                                @if($data->is_text == true)
                                <tr>
                                    <th>Title</th>
                                    <td>{!! $data->title !!}</td>
                                </tr>
                                <tr>
                                    <th>Caption 1</th>
                                    <td>{!! $data->caption_1 !!}</td>
                                </tr>
                                <tr>
                                    <th>Caption 2</th>
                                    <td>{!! $data->caption_2 !!}</td>
                                </tr>
                                <tr>
                                    <th>Caption 3</th>
                                    <td>{!! $data->caption_3 !!}</td>
                                </tr>
                                @endif
                                <tr>
                                    <th>Button</th>
                                    <th>
                                        {!! $data->is_button == true ? 'Yes' : 'No' !!}
                                    </th>
                                </tr>
                                @if($data->is_button == true)
                                <tr>
                                    <th>Button Title</th>
                                    <td>{!! $data->btn_title !!}</td>
                                </tr>
                                <tr>
                                    <th>Button URL</th>
                                    <td>{!! $data->btn_url !!}</td>
                                </tr>
                                @endif
                                <tr>
                                    <th>Order</th>
                                    <td>{!! $data->order !!}</td>
                                </tr>
                                <tr>
                                    <th>Active</th>
                                    <th>{!! $data->is_active == true ? 'Yes' : 'No' !!}</th>
                                </tr>
                                <tr>
                                    <th>Created Date</th>
                                    <td>{!! date('d-m-Y H:i:s', strtotime($data->created_at)) !!}</td>
                                </tr>
                                <tr>
                                    <th>Updated Date</th>
                                    <td>{!! date('d-m-Y H:i:s', strtotime($data->updated_at)) !!}</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection