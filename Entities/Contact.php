<?php

namespace Modules\CMS\Entities;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    protected $table = 'cms_contact';
    protected $fillable = [
        'company_name',
        'building_name',
        'street_name',
        'street_area',
        'phone',
        'fax',
        'email',
        'is_branch',
        'has_map',
        'longitude',
        'latitude',
        'zoom',
        'is_active'
    ];
}
