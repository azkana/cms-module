<?php

namespace Modules\CMS\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\CMS\Entities\Info;

class InfoController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $params['pageTitle']    = 'Info';
        $params['pageSubTitle'] = 'Index';
        $params['data']         = Info::all();
        return view('cms::info.index', $params);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $params['pageTitle']    = 'Info';
        $params['pageSubTitle'] = 'Create';
        return view('cms::info.create', $params);
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        try {
            $data['title']          = trim($request->title);
            $data['slug']           = trim($request->slug);
            $data['description']    = trim($request->description);
            $data['iframe_url']     = trim($request->iframe_url);
            $data['iframe_width']   = $request->iframe_width;
            $data['iframe_height']  = $request->iframe_height;
            $data['footnote']       = trim($request->footnote);
            $data['is_active']      = $request->is_active;
            $data['author']         = Auth()->user()->id;

            Info::create($data);
            return redirect(route('info.index'))
				->with('success_message', trans('message.success_save'));
        } catch(\Exception $e) {
            return redirect(route('info.index'))
				->with('error_message', trans('message.error_save'));
        }
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('cms::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id)
    {
        $params['pageTitle']    = 'Info';
        $params['pageSubTitle'] = 'Create';
        $params['data']         = Info::findOrFail($id);
        return view('cms::info.edit', $params);
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request, $id)
    {
        try {
            $info = Info::findOrFail($id);
            $data['title']          = trim($request->title);
            $data['slug']           = trim($request->slug);
            $data['description']    = trim($request->description);
            $data['iframe_url']     = trim($request->iframe_url);
            $data['iframe_width']   = $request->iframe_width;
            $data['iframe_height']  = $request->iframe_height;
            $data['footnote']       = trim($request->footnote);
            $data['is_active']      = $request->is_active == 1 ? 1 : 0;

            $info->update($data);
            return redirect(route('info.index'))
				->with('success_message', trans('message.success_edit'));
        } catch(\Exception $e) {
            return redirect(route('info.index'))
				->with('error_message', trans('message.error_edit'));
        }
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id)
    {
        try {
            $data = Info::findOrFail($id);
            $data->delete();
            return redirect(route('info.index'))
				->with('success_message', trans('message.success_delete'));
        } catch (\Exception $e) {
            return redirect(route('info.index'))
				->with('error_message', trans('message.error_delete'));
        }
    }
}
