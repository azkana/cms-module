<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cms_info', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title', 100)->comment('Title');
            $table->string('slug', 100)->comment('Slug');
            $table->text('description', 1000)->comment('Description');
            $table->string('iframe_url', 200)->nullable()->comment('URL');
            $table->integer('iframe_width')->comment('Iframe width');
            $table->integer('iframe_height')->comment('Iframe height');
            $table->boolean('is_active')->comment('Status');
            $table->string('footnote', 500)->nullable()->comment('footnote');
            $table->integer('author')->comment('Author');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cms_info');
    }
}
