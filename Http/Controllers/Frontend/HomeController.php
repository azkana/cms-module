<?php

namespace Modules\CMS\Http\Controllers\Frontend;

use Illuminate\Routing\Controller;
use Modules\CMS\Entities\AboutUs;
use Modules\CMS\Entities\Contact;
use Modules\CMS\Entities\Service;
use Modules\CMS\Entities\Client;
use Modules\CMS\Entities\Slider;
use Modules\CMS\Entities\Info;
use Modules\CMS\Entities\Jobs;
use Modules\CMS\Entities\Video;

class HomeController extends Controller
{
    public function index()
    {
        $params['pageTitle']    = 'Lowongan Terbaru di Bekasi | Jobsdku.co.id';
        $params['metaDescription'] = 'Dapatkan informasi lowongan terbaru di Bekasi dan sekitarnya.';
        /* Slider */
        $params['sliders']      = Slider::where('is_active', true)
            ->orderBy('order', 'ASC')
            ->orderBy('created_at', 'DESC')
            ->get();
        $params['seq']          = 0;
        $params['sequ']         = 0;
        $params['slider_no']    = 0;
        /* Videos */
        $params['videos']       = Video::where('is_active', true)->orderBy('order', 'ASC')->limit(1)->get();
        /* About Us */
        $params['about']        = AboutUs::findOrFail(1);
        /*Services */
        $params['services']     = Service::where('is_active', true)
                ->orderBy('order', 'ASC')->get();
        return view('cms::frontend.index', $params);
    }

    public function info() {
        $params['pageTitle']    = 'Informasi Terbaru | Jobsdku.co.id';
        $params['info']         = Info::where('is_active', true)
            ->orderBy('created_at', 'DESC')
            ->get();
        return view('cms::frontend.info', $params);
    }

    public function infoDetail($id) {
        $params['data']         = $data = Info::where('slug', $id)->firstOrFail();
        $params['pageTitle']    = $data->title . ' | Jobsdku.co.id';
        return view('cms::frontend.info-detail', $params);
    }

    public function contactUs() {
        $params['pageTitle']    = 'Kontak | Jobsdku.co.id';
        $params['data']         = Contact::all();
        $params['headOffice']   = Contact::where('is_branch', 0)->firstOrFail();
        return view('cms::frontend.contact', $params);
    }

    public function client() {
        $params['pageTitle']    = 'Klien | Jobsdku.co.id';
        $params['data']         = Client::all();
        $params['category']     = Client::select('category')->distinct()->orderBy('category', 'asc')->get();
        return view('cms::frontend.clients', $params);
    }

    public function jobs() {
        $params['pageTitle']    = 'Lowongan Pekerjaan Terbaru ' . date('Y') .' | Jobsdku.co.id';
        $params['data']         = Jobs::where('is_active', 1)->orderBy('created_at', 'DESC')->get();
        return view('cms::frontend.jobs', $params);
    }

    public function jobsDetail($slug) {
        $params['data']         = $data = Jobs::where('slug', $slug)->firstOrFail();
        $params['pageTitle']    = 'Lowongan ' . $data->title . ' | Jobsdku.co.id';
        return view('cms::frontend.jobs-detail', $params);
    }

    public function videoShow($id) {
        $params['pageTitle']    = 'Video | Jobsdku.co.id';
        $params['data'] = Video::find($id);
        return view('cms::frontend.video-show', $params);
    }

    public function test()
    {
        $params['pageTitle']    = 'Test | Jobsdku.co.id';
        $params['type']         = array(
            'nik' => 'No. KTP',
            'member' => 'No. Member'
        );
        return view('cms::frontend.test', $params);
    }

    public function testResult()
    {
        $params['pageTitle']    = 'Test | Jobsdku.co.id';
        $params['type']         = array(
            'nik' => 'No. KTP',
            'member' => 'No. Member'
        );
        return view('cms::frontend.test-result', $params);
    }

}
