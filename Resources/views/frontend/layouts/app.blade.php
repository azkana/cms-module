<!DOCTYPE html>
<html lang="id">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport" />
        <meta name="google-site-verification" content="mL8tZXzP8lFQU1Pz7y3-vYkhqS5kYrxJ8hKt86cMoOM" />
        <meta name="robots" content="noodp">

        <title> {!! isset($pageTitle) ? $pageTitle : null !!} </title>
        
        <meta name="title" content="{!! isset($pageTitle) ? $pageTitle : null !!}">
        <meta name="description" content="{!! isset($metaDescription) ? $metaDescription : null !!}" />

        {{-- Open Graph / Facebook --}}
        <meta property="og:type" content="website">
        <meta property="og:url" content="http://www.jobsdku.co.id/">
        <meta property="og:title" content="{!! isset($pageTitle) ? $pageTitle : null !!}">
        <meta property="og:description" content="{!! isset($metaDescription) ? $metaDescription : null !!}">
        <meta property="og:image" content="">

        {{-- Twitter --}}
        <meta property="twitter:card" content="summary_large_image">
        <meta property="twitter:url" content="http://www.jobsdku.co.id/">
        <meta property="twitter:title" content="{!! isset($pageTitle) ? $pageTitle : null !!}">
        <meta property="twitter:description" content="{!! isset($metaDescription) ? $metaDescription : null !!}">
        <meta property="twitter:image" content="">

        
        <link rel="icon" href="{!! asset('modules/cms/frontend/img/logo-dku.png') !!}">
        <link href="{!! asset('modules/cms/frontend/plugins/bootstrap/css/bootstrap.min.css') !!}" rel="stylesheet">
        <link href="{!! asset('modules/cms/frontend/plugins/bootstrap/css/carousel.css') !!}" rel="stylesheet">
        <link href="{!! asset('modules/cms/frontend/plugins/bootstrap/css/sticky-footer-navbar.css') !!}" rel="stylesheet">
        <link href="{!! asset('plugins/font-awesome/4.5.0/css/font-awesome.min.css') !!}" rel="stylesheet">
        <link href="{!! asset('plugins/ionicons/css/ionicons.min.css') !!}" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
        <link href='http://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>
        <link href="https://fonts.googleapis.com/css2?family=Righteous&display=swap" rel="stylesheet">
        
        <style>
            body {
                font-family: 'Roboto', sans-serif;
                min-height: 75rem;
                padding-top: 4.5rem;
            }

            h1,h2,h3,h4,h5,h6 {
                font-family: 'Righteous', cursive;
            }

            .topnav {
                font-family: 'Righteous', cursive;
            }

            p {
                font-size: 16px;
                font-family: 'Lato', sans-serif;
            }
        </style>
        @yield('css')

        <script type="text/javascript" src="https://platform-api.sharethis.com/js/sharethis.js#property=5fbb79574ec2e100127e0bdf&product=inline-share-buttons" async="async"></script>
        {{-- Global site tag (gtag.js) - Google Analytics --}}
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-163105943-1"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag() {
                dataLayer.push(arguments);
            }
            gtag('js', new Date());
            gtag('config', 'UA-163105943-1');
        </script>
    </head>
    <body>
        
        @include('cms::frontend.shared.header')

        @yield('content')

        @include('cms::frontend.shared.footer')

        <script src="{!! asset('modules/cms/frontend/plugins/jQuery/jquery-2.2.3.min.js') !!}"></script>
        <script src="{!! asset('modules/cms/frontend/plugins/bootstrap/js/bootstrap.min.js') !!}"></script>
        @yield('js')

    </body>
</html>