<?php

namespace Modules\CMS\Entities;

use Illuminate\Database\Eloquent\Model;

class AboutUs extends Model
{
    protected $table = 'cms_about_us';
    protected $fillable = [
        'title',
        'description',
        'image'
    ];
}
