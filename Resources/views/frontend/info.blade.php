@extends('cms::frontend.layouts.app')

@section('content')

	<div class="jumbotron page-banner">
		<div class="container">
			<h2 style="padding-top: 30px"><i>Info Terbaru</i></h2>
		</div>
	</div>

	<div class="info-lists">
		<div class="container" style="padding-top:10px">
			<div class="row">
				@foreach ($info as $row)
				<div class="col-md-4">
					<div class="card">
						<h4>
							<a href="{!! route('cms.info.detail', $row->slug) !!}">
								{!! $row->title !!}
							</a>
						</h4>
						<span>
							<small>
								<i class="glyphicon glyphicon-time"></i> {!! date( 'd M Y', strtotime($row->created_at)) !!}
							</small>
						</span>
						<p>{!! $row->info_excerpt !!}</p>
					</div>
				</div>
				@endforeach
			</div>
		</div>
	</div>

@endsection

@section('css')

	<style>
		.page-banner {
			background-image: linear-gradient(rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)), url('{!! asset('modules/cms/frontend/img/news-banner.jpg') !!}');
			background-repeat: no-repeat;
			background-size: cover;
			background-position: center;
			color: #ffffff;
		}

		.info-lists {
			margin-bottom:20px;
   			padding:20px 0px;
		}

		.info-lists .row {
			margin-top:20px;
		}

		.info-lists a {
			color: black;
		}

		.info-lists a:hover {
			color:black;
			text-decoration:none;
		}

		.info-lists .card {
			background-color: #FFFFFF;
			padding:20px;
			-webkit-border-radius: 4px;
			-moz-border-radius: 4px;
			border-radius:4px;
			box-shadow: 0 4px 5px 0 rgba(0,0,0,0.14), 0 1px 10px 0 rgba(0,0,0,0.12), 0 2px 4px -1px rgba(0,0,0,0.3);
			height: 300px;
			opacity: 0.65;
		}

		.info-lists .card:hover {
			box-shadow: 0 16px 24px 2px rgba(0,0,0,0.14), 0 6px 30px 5px rgba(0,0,0,0.12), 0 8px 10px -5px rgba(0,0,0,0.3);
			color:black;
		}
	</style>

@endsection