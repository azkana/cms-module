@extends('cms::frontend.layouts.app')

@section('content')

	<div class="jumbotron page-banner">
		<div class="container">
			<h2 style="padding-top: 30px"><i>Lowongan Kerja</i></h2>
		</div>
	</div>

	<div class="container" style="padding-top:10px">
		{{-- <div class="row">
			<div class="col-md-6 col-lg-offset-3">
				<div class="input-group">
					{!! Form::text('search_job', null, ['class' => 'form-control', 'id' => 'search_job']) !!}
					<span class="input-group-btn">
						<button class="btn btn-default" type="button">Cari</button>
					</span>
				</div>
			</div>
		</div> --}}
		<div class="row" style="margin-top: 20px">
			@foreach ($data as $row)
		  	<div class="col-md-8 col-lg-offset-2">
			  	<div class="panel panel-default">
				  	<div class="panel-body">
					  	<h4>
						  	<b>
							  	<a href="{!! route('cms.job.detail', $row->slug) !!}">
								  	{!! $row->title !!}
							  	</a>
						  	</b>
						</h4>
						<span>
							@if(!empty($row->company))
							{!! $row->company !!}
							- 
							@endif
							@if(!empty($row->location))
							<i class="glyphicon glyphicon-map-marker"></i> {!! $row->location !!}
							@endif
						</span><br>
						<span title="Posted">
							<small>
								<i class="glyphicon glyphicon-time"></i> {!! date( 'd M Y', strtotime($row->created_at)) !!}
							</small>
						</span><hr>
						<span>
							{!! $row->job_excerpt !!}
							<a href="{!! route('cms.job.detail', $row->slug) !!}"><i>selengkapnya</i></a>
						</span>
				  	</div>
			  	</div>
		  	</div>
			@endforeach
		</div>
  	</div>

@endsection

@section('css')

	<style>
		.page-banner {
			background-image: linear-gradient(rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)), url('{!! asset('modules/cms/frontend/img/jobs-banner.jpg') !!}');
			background-repeat: no-repeat;
			background-size: cover;
			background-position: center;
			color: #ffffff;
		}
	</style>

@endsection