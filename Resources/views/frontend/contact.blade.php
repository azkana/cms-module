@extends('cms::frontend.layouts.app')

@section('content')

	<div class="jumbotron page-banner">
		<div class="container">
			<h2 style="padding-top: 30px; z-index:200"><i>{!! $pageTitle !!}</i></h2>
		</div>
	</div>

	<div class="container" style="padding-top:5px">
		<div class="row">
			<div class="col-md-4">
			  	@if($data->count() > 0)
			  	<h5 class="">Head Office</h5>
			  	<table class="table table-hover table-borderless">
				  	<tr>
					  	<th>
						  	<i class="fa fa-building text-primary"></i>
					  	</th>
					  	<td>{!! $headOffice->company_name !!}</td>
				  	</tr>
				  	<tr>
					  	<th>
						  	<i class="fa fa-map-marker text-danger" style="font-size: 2rem"></i>
					  	</th>
					  	<td>
						  	{!! $headOffice->building_name !!},
						  	{!! $headOffice->street_name !!},
						  	{!! $headOffice->street_area !!},
					  	</td>
				  	</tr>
				  	<tr>
					  	<th>
						  	<i class="glyphicon glyphicon-phone-alt text-info"></i>
					  	</th>
					  	<td>{!! $headOffice->phone !!}</td>
				  	</tr>
				  	{{-- <tr>
					  	<th>
						 	<i class="fa fa-fax text-warning"></i>
					  	</th>
					  	<td>{!! $headOffice->fax !!}</td>
				  	</tr> --}}
				  	<tr>
					  	<th>
						  	<i class="fa fa-envelope text-success"></i>
					  	</th>
					  	<td>
						  	<a href="mailto:{!! $headOffice->email !!}">
							  	{!! $headOffice->email !!}
						  	</a>
					  	</td>
				  	</tr>
			  	</table>
			  	@endif
		  	</div>
			<div class="col-md-8">
			  	@if($data->count() > 0)
				  	@if(!empty($headOffice->latitude) && !empty($headOffice->longitude))
					  	<div id="office-map" style="width: 100%; height: 500px;"></div>
				  	@endif
			  	@endif
		  	</div>
		</div>
  	</div>

@endsection

@section('css')

	<style>
		.page-banner {
			background-image: linear-gradient(rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)), url('{!! asset('modules/cms/frontend/img/contact-us-banner.jpg') !!}');
			background-repeat: no-repeat;
			background-size: cover;
			background-position: center;
			color: #ffffff;
		}
		.table-borderless td,
		.table-borderless th {
			border: 0 !important;
		}
	</style>

@endsection

@section('js')

	<script src="https://maps.googleapis.com/maps/api/js?key={!! getOptions('maps_api_key') !!}"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/gmaps.js/0.4.24/gmaps.js"></script>
	@if($data->count() > 0)
	<script type="text/javascript">
		var headOfficeLat = {!! $headOffice->latitude !!};
		var headOfficeLng = {!! $headOffice->longitude !!};
		var headOfficeZom = {!! $headOffice->zoom !!};
		var headOfficeMap = new GMaps({
			el: '#office-map',
			lat: headOfficeLat,
			lng: headOfficeLng,
			zoom: headOfficeZom,
			disableDefaultUI: true,
			disableDoubleClickZoom: true,
			scaleControl: true,
			fullscreenControl: true,
			zoomControl: true,
			zoomControlOptions: {
				position: google.maps.ControlPosition.RIGHT_TOP
			},
			gestureHandling: 'cooperative'
		});
		headOfficeMap.addMarker({
			lat: headOfficeLat,
			lng: headOfficeLng,
			title: '{!! $headOffice->company_name !!}',
			infoWindow: {
				content: '<h5>Head Office</h5><br>' + '<p><b>{!! $headOffice->company_name !!}</b></p>' +
					'<p>{!! $headOffice->building_name !!}, {!! $headOffice->street_name !!}, <br>{!! $headOffice->street_area !!}</p>' +
					'<p><i class="glyphicon glyphicon-phone-alt"></i> {!! $headOffice->phone !!} <i class="fa fa-fax"></i> {!! $headOffice->fax !!}</p>' +
					'<p><i class="fa fa-envelope"></i> {!! $headOffice->email !!}</p>'
			}
		});
	</script>
	@endif

@endsection