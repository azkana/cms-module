<?php

namespace Modules\CMS\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Storage;
use Modules\CMS\Entities\Client;

class ClientController extends Controller
{
    protected $pageTitle;
    protected $disk;

    public function __construct()
    {
        $this->pageTitle = 'Klien';
        $this->disk = config('cms.disk');
        setLocale(LC_TIME, 'id');
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $params['pageTitle']    = $this->pageTitle;
        $params['data']         = Client::latest()->get();
        return view('cms::client.index', $params);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $params['pageTitle']    = $this->pageTitle;
        return view('cms::client.create', $params);
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        try {
            $data['company_name']   = trim($request->company_name);
            $data['alias_name']     = trim($request->alias_name);
            $data['category']       = trim($request->category);
            $data['website']        = trim($request->website);

            if($request->hasFile('logo_img')) {
                $fileExt    = strtolower($request->file('logo_img')->getClientOriginalExtension());
                $fileName   = 'client-' . md5(date('Y-m-d').time()) . '.' . $fileExt;
                $data['logo_img']   = $fileName;
                Storage::disk($this->disk)->put(
                    'cms/clients/' . $fileName,
                    fopen($request->file('logo_img'), 'r+'),
                    'public'
                );
            }

            Client::create($data);
            return redirect(route('client.index'))
				->with('success', trans('message.success_save'));
        } catch (\Exception $e) {
            return redirect(route('client.index'))
				->with('error', trans('message.error_save'));
        }
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id)
    {
        $params['pageTitle']    = $this->pageTitle;
        $params['data']         = Client::findOrFail($id);
        return view('cms::client.edit', $params);
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request, $id)
    {
        try {
            $client = Client::findOrFail($id);
            $data['company_name']   = trim($request->company_name);
            $data['alias_name']     = trim($request->alias_name);
            $data['category']       = trim($request->category);
            $data['website']        = trim($request->website);

            if($request->hasFile('logo_img')) {
                $fileExt    = strtolower($request->file('logo_img')->getClientOriginalExtension());
                $fileName   = 'client-' . md5(date('Y-m-d').time()) . '.' . $fileExt;
                $data['logo_img']   = $fileName;
                Storage::disk($this->disk)->put(
                    'cms/clients/' . $fileName,
                    fopen($request->file('logo_img'), 'r+'),
                    'public'
                );
            }

            $client->update($data);
            return redirect(route('client.index'))
				->with('success', trans('message.success_edit'));
        } catch (\Exception $e) {
            return redirect(route('client.index'))
				->with('error', trans('message.error_edit'));
        }
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id)
    {
        try {
            $data = Client::findOrFail($id);
            if(!empty($data->logo_img)) {
                Storage::disk($this->disk)->delete('cms/clients/' . $data->logo_img);
            }
            $data->delete();
            return redirect(route('client.index'))
				->with('success', trans('message.success_delete'));
        } catch (\Exception $e) {
            return redirect(route('client.index'))
				->with('error', trans('message.error_delete'));
        }
    }
}
