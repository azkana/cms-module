<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cms_client', function (Blueprint $table) {
            $table->increments('id');
            $table->string('company_name', 100)->nullable()->comment('Nama Perusahaan');
            $table->string('alias_name', 20)->nullable()->comment('Nama Alias Perusahaan');
            $table->string('category', 200)->nullable()->comment('Kategori Perusahaan');
            $table->string('website', 50)->nullable()->comment('Website Perusahaan');
            $table->string('logo_img', 100)->nullable()->comment('Logo Perusahaan');
            $table->boolean('is_active')->default(1)->comment('Status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cms_client');
    }
}
