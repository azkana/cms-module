@extends('cms::layouts.master')

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title"></h3>
                    <div class="box-tools pull-right">
                        <a class="btn btn-sm btn-primary" href="{!! route('about.edit', 1) !!}">
                            <i class="fa fa-edit"></i> Edit
                        </a>
                    </div>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <table class="table table-bordered table-hover">
                                <tr>
                                    <th width="30%">Title</th>
                                    <td>{!! $data->title !!}</td>
                                </tr>
                                <tr>
                                    <th>Description</th>
                                    <td>{!! $data->description !!}</td>
                                </tr>
                                <tr>
                                    <th>Image</th>
                                    <td>
                                        <img src="{!! asset('storage/about/' . $data->image) !!}" style="max-width: 300px; border-radius: 10px" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection