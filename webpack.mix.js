let mix = require('laravel-mix');

/* Allow multiple Laravel Mix applications*/
require('laravel-mix-merge-manifest');
mix.mergeManifest();

mix.js('resources/assets/js/app.js', 'public/js')
   .sass('resources/assets/sass/app.scss', 'public/css');

mix.styles([
    __dirname + '/Resources/assets/frontend/plugins/bootstrap/css/bootstrap.css',
    __dirname + '/Resources/assets/frontend/plugins/bootstrap/css/carousel.css',
], 'public/css/style.css');