<?php

namespace Modules\CMS\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\CMS\Entities\AboutUs;
use Illuminate\Support\Facades\Storage;

class AboutUsController extends Controller
{
    protected $disk;

    public function __construct()
    {
        $this->disk = config('cms.disk');
    }
    
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $params['pageTitle']    = 'About Us';
        $params['data']         = AboutUs::findOrFail(1);
        return view('cms::about.index', $params);
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        $params['pageTitle']    = 'About Us';
        $params['data']         = AboutUs::findOrFail(1);
        return view('cms::about.edit', $params);
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request, $id)
    {
        try {
            $about              = AboutUs::findOrFail($id);
            $data['title']      = trim($request->title);
            $data['description']= trim($request->description);

            if($request->hasFile('image')) {
                $fileExt    = strtolower($request->file('image')->getClientOriginalExtension());
                $fileName   = 'about-us' . '.' . $fileExt;
                $data['image']   = $fileName;
                Storage::disk($this->disk)->put(
                    'cms/about/' . $fileName,
                    fopen($request->file('image'), 'r+'),
                    'public'
                );
            }

            $about->update($data);
            return redirect(route('about.index'))
				->with('success_message', trans('message.success_edit'));
        } catch(\Exception $e) {
            return redirect(route('about.index'))
				->with('error_message', trans('message.error_edit'));
        }
    }
}
