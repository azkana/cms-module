<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCmsJobsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cms_jobs', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('title', 100)->comment('Job Title');
            $table->string('slug', 100)->comment('Slug');
            $table->text('desc', 1000)->nullable()->comment('Job Description');
            $table->string('skills', 200)->nullable()->comment('Job Skill');
            $table->string('location', 100)->nullable()->comment('Job Location');
            $table->string('degree_level', 100)->nullable()->comment('Job Degree Level');
            $table->integer('experience')->nullable()->comment('Job Experience');
            $table->string('company', 100)->nullable()->comment('Company');
            $table->double('salary')->nullable()->comment('Salary');
            $table->date('expired')->nullable()->comment('Job Expired');
            $table->boolean('is_active')->default(0)->comment('Active Status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cms_jobs');
    }
}
