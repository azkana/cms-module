<?php

namespace Modules\CMS\Entities;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    protected $table = 'cms_client';
    protected $fillable = [
        'company_name',
        'alias_name',
        'category',
        'website',
        'logo_img',
        'is_active',
    ];
}
