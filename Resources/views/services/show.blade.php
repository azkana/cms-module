@extends('cms::layouts.master')

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title"></h3>
                    <div class="box-tools pull-right">
                        <a class="btn btn-sm btn-danger" href="{!! route('service.index') !!}">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="box-body" style="min-height: 520px">
                    <div class="row">
                        <div class="col-sm-12">
                            <table class="table table-bordered table-hover">
                                <tr>
                                    <th width="30%">Title</th>
                                    <td>{!! $data->title !!}</td>
                                </tr>
                                <tr>
                                    <th>Description</th>
                                    <td>{!! $data->description !!}</td>
                                </tr>
                                <tr>
                                    <th>Image</th>
                                    <td>
                                        <img src="{!! asset('storage/services/' . $data->image) !!}" style="max-width: 300px; border-radius: 10px" />
                                    </td>
                                </tr>
                                <tr>
                                    <th>Order</th>
                                    <td>{!! $data->order !!}</td>
                                </tr>
                                <tr>
                                    <th>Status</th>
                                    <td>{!! $data->is_active == true ? 'Active' : 'Disable' !!}</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection