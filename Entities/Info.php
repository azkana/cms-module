<?php

namespace Modules\CMS\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Info extends Model
{
    protected $table = 'cms_info';
    protected $fillable = [
        'title',
        'slug',
        'description',
        'iframe_url',
        'iframe_width',
        'iframe_height',
        'footnote',
        'is_active',
        'author'
    ];

    public function getInfoExcerptAttribute()
    {
        return Str::words(strip_tags($this->description), 20, ' ...');
    }
}
