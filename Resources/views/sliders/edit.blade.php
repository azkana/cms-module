@extends('cms::layouts.master')

@section('content')
    {!! Form::open([ 'route' => ['slider.update', $data->id], 'method' => 'patch', 'class' => 'form-horizontal', 'files' => true, 'autocomplete' => 'off' ]) !!}
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Slider</h3>
                        <div class="box-tools pull-right">
                            <a class="btn btn-sm btn-danger" href="{!! route('slider.index') !!}">
                                <i class="fa fa-times"></i>
                            </a>
                        </div>
                    </div>
                    <div class="box-body" style="min-height: 470px">
                        {{-- Jenis Slider --}}
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Jenis Slider </label>
                                    <div class="col-sm-2">
                                        <div class="checkbox">
                                            <label>
                                                {!! Form::checkbox('is_image', true, $data->is_image == true ? true : false, ['id' => 'is_image']) !!} Gambar
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-sm-2">
                                        <div class="checkbox">
                                            <label>
                                                {!! Form::checkbox('is_text', true, $data->is_text == true ? true : false, ['id' => 'is_text']) !!} Text
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-sm-2 is-button-group">
                                        <div class="checkbox">
                                            <label>
                                                {!! Form::checkbox('is_button', true, $data->is_button == true ? true : false, ['id' => 'is_button']) !!} Button
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                
                            </div>
                        </div>
                        {{-- Image --}}
                        <hr class="divider is-image" />
                        <div class="row is-image">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="slider_img" class="col-sm-4 control-label">Gambar <span class="required">*</span></label>
                                    <div class="col-sm-6">
                                        {!! Form::file('slider_img', [ 'class' => 'form-control', 'id' => 'slider_img', 'accept' => '.jpg,.png', empty($data->slider_img) ? 'required' : null]) !!}
                                        <p class="help-block small">
                                            - best dimension 1080 x 400 <br>
                                            - max size 2 MB<br>
                                            - filetype jpg, png
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="col-sm-12">
                                    @if(!empty($data->slider_img))
                                    <img src="{!! _get_file($data->slider_img, '/cms/sliders/', config('cms.disk')) !!}" style="max-height: 100px" />
                                    @endif
                                </div>
                            </div>
                        </div>
                        {{-- Text & Button --}}
                        <hr class="divider is-text" />
                        <div class="row">
                            <div class="col-sm-6 is-text">
                                <div class="form-group">
                                    <label for="title" class="col-sm-4 control-label">Headline <span class="required">*</span></label>
                                    <div class="col-sm-6">
                                        {!! Form::text('title', $data->title, [ 'class' => 'form-control', 'id' => 'title', 'maxlength' => 100, 'required']) !!}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="caption_1" class="col-sm-4 control-label">Caption 1 <span class="required">*</span></label>
                                    <div class="col-sm-6">
                                        {!! Form::text('caption_1', $data->caption_1, [ 'class' => 'form-control', 'id' => 'caption_1', 'maxlength' => 100, 'required']) !!}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="caption_2" class="col-sm-4 control-label">Caption 2</label>
                                    <div class="col-sm-6">
                                        {!! Form::text('caption_2', $data->caption_2, [ 'class' => 'form-control', 'maxlength' => 100]) !!}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="caption_3" class="col-sm-4 control-label">Caption 3</label>
                                    <div class="col-sm-6">
                                        {!! Form::text('caption_3', $data->caption_3, [ 'class' => 'form-control', 'maxlength' => 100]) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 is-button">
                                <div class="form-group">
                                    <label for="btn_title" class="col-sm-4 control-label">Button Title</label>
                                    <div class="col-sm-6">
                                        {!! Form::text('btn_title', $data->btn_title, [ 'class' => 'form-control', 'id' => 'btn_title']) !!}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="btn_url" class="col-sm-4 control-label">Button URL</label>
                                    <div class="col-sm-6">
                                        {!! Form::text('btn_url', $data->btn_url, [ 'class' => 'form-control', 'id' => 'btn_url']) !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr class="divider" />
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="order" class="col-sm-4 control-label">Order</label>
                                    <div class="col-sm-3">
                                        {!! Form::number('order', $data->order, [ 'class' => 'form-control']) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="is_active" class="col-sm-4 control-label">Active? </label>
                                    <div class="col-sm-6">
                                        <div class="checkbox">
                                            <label>
                                                {!! Form::checkbox('is_active', true, $data->is_active == true ? true : false) !!} Yes
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        <button type="reset" class="btn btn-default">Cancel</button>
                        <button type="submit" class="btn btn-info pull-right" id="btn-save">Save</button>
                    </div>
                </div>
            </div>
        </div>
    {!! Form::close() !!}
@endsection

@section('scripts')
    <script>
        $(document).ready(function() {
            var isImageValue    = {!! $data->is_image !!};
            var isTextValue     = {!! $data->is_text !!};
            var isButtonValue   = {!! $data->is_button !!};
            var isImage = $("#is_image");
            var isText  = $("#is_text");
            var isButton= $("#is_button");

            if(isImageValue === 0) {
                $(".is-image").addClass('hide');
                $('#slider_img').prop('required', '');
            }

            if(isTextValue === 0) {
                $(".is-text").addClass('hide');
                $('#title').prop('required', '');
                $('#caption_1').prop('required', '');
            }

            if(isButtonValue === 0) {
                $(".is-button").addClass('hide');
            }

            isImage.on('click', function() {
                if($(this).prop('checked') === true) {
                    $('.is-image').removeClass('hide');
                    $('#slider_img').prop('required', 'required');
                } else {
                    $('.is-image').addClass('hide');
                    $('#slider_img').prop('required', '');
                }
                showHideButton();
            });

            isText.on('click', function() {
                if($(this).prop('checked') === true) {
                    $('.is-text').removeClass('hide');
                    $('#title').prop('required', 'required');
                    $('#caption_1').prop('required', 'required');
                } else {
                    $('.is-text').addClass('hide');
                    $('#title').prop('required', '');
                    $('#caption_1').prop('required', '');
                }
                showHideButton();
            });

            isButton.click(function() {
                showHideIsButton($(this));
            });

            function showHideIsButton(btn) {
                if(btn.prop('checked') === true) {
                    $(".is-button").removeClass('hide');
                } else {
                    $(".is-button").addClass('hide');
                }
            }

            function showHideButton() {
                if(isImage.prop('checked') !== true && isText.prop('checked') !== true) {
                    $('.is-button-group').addClass('hide');
                    $(".is-button").addClass('hide');
                    $('#btn-save').prop('disabled', 'disabled');
                } else {
                    $('.is-button-group').removeClass('hide');
                    $('#btn-save').prop('disabled', '');
                    showHideIsButton(isButton);
                }
            }
            
        });
        
        $(function () {
            $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
                checkboxClass: 'icheckbox_minimal-blue',
                radioClass: 'iradio_minimal-blue'
            });
        });
    </script>
@endsection