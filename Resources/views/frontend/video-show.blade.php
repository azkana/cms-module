@extends('cms::frontend.layouts.app')

@section('css')
	<link rel="stylesheet" href="https://cdn.plyr.io/3.6.4/plyr.css" />
@endsection

@section('content')

	<div class="col-md-12" style="margin-top: 50px; text-align:center">
		<div class="col-md-12 col-xs-12 col-lg-4 col-lg-offset-4">
			<h3><b>{!! $data->title !!}</b></h3>
			<span>
				<small>
					<i class="glyphicon glyphicon-time"></i> {!! dateFormatDmyHi($data->created_at) !!}
				</small>
			</span>
			<br><br>
			<video autoplay alt="{!! $data->title !!}" id="player">
				<source src="{!! asset('/storage/videos/' . $data->file_name) !!}" type="video/mp4">
				{{-- <source src="{!! config('filesystems.disks.s3.url') . '/cms/videos/' . $video->file_name !!}" type="video/mp4"> --}}
				Your browser does not support the video tag.
			</video>
		</div>
	</div>

@endsection

@section('js')
	<script src="https://cdn.plyr.io/3.6.4/plyr.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			const player = new Plyr('#player', {
				controls: ['play', 'progress', 'mute', 'volume', 'pip', 'fullscreen'],
				autopause: true,
				resetOnEnd: true
			});
		});
	</script>
@endsection