@extends('cms::layouts.master')

@section('content')
    {!! Form::open([ 'route' => ['video.update', $data->id], 'method' => 'patch', 'class' => 'form-horizontal', 'files' => false, 'autocomplete' => 'off' ]) !!}
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Edit Video</h3>
                        <div class="box-tools pull-right">
                            <a class="btn btn-sm btn-danger" href="{!! route('video.index') !!}">
                                <i class="fa fa-times"></i>
                            </a>
                        </div>
                    </div>
                    <div class="box-body pad">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="title" class="col-sm-2 control-label">Title <span class="required">*</span></label>
                                    <div class="col-sm-8">
                                        {!! Form::text('title', $data->title, [ 'class' => 'form-control', 'id' => 'title', 'maxlength' => 100, 'required', 'autofocus']) !!}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="video_file" class="col-sm-2 control-label">Video <span class="required">*</span></label>
                                    <div class="col-sm-8">
                                        <video controls style="max-width: 360px; max-height: 240px;" alt="{!! $data->title !!}">
                                            <source src="{!! config('filesystems.disks.s3.url') . '/cms/videos/' . $data->file_name !!}" type="video/mp4">
                                            Your browser does not support the video tag.
                                        </video>
                                        {{-- {!! Form::file('video_file', [ 'class' => 'form-control', 'id' => 'video_file', 'accept' => '.mp4', 'required']) !!}
                                        <p class="help-block small">
                                            - max size 20 MB<br>
                                            - filetype mp4
                                        </p> --}}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="is_active" class="col-sm-2 control-label">Active? </label>
                                    <div class="col-sm-8">
                                        <div class="checkbox">
                                            <label>
                                                {!! Form::checkbox('is_active', true, $data->is_active == true ? true : false) !!} Yes
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="is_active" class="col-sm-2 control-label">Order </label>
                                    <div class="col-sm-2">
                                        {!! Form::number('order', $data->order, [ 'class' => 'form-control']) !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        <button type="reset" class="btn btn-default">Cancel</button>
                        <button type="submit" class="btn btn-info pull-right">Save</button>
                    </div>
                </div>
            </div>
        </div>
    {!! Form::close() !!}
@endsection

@section('scripts')
    <script>
        $(document).ready(function() {
            
        });
        $(function () {
            $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
                checkboxClass: 'icheckbox_minimal-blue',
                radioClass: 'iradio_minimal-blue'
            });
        });
    </script>
@endsection