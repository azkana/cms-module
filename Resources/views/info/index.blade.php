@extends('cms::layouts.master')

@section('content')

    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title"></h3>
                    <div class="box-tools pull-right">
                        {{-- @can('info-create') --}}
                        <a class="btn btn-sm btn-primary" href="{!! route('info.create') !!}">
                            <i class="fa fa-plus"></i> Info Baru
                        </a>
                        {{-- @endcan --}}
                    </div>
                </div>
                <div class="box-body" style="min-height: 520px">
                    <table id="grid-info" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th style="width: 5%">No.</th>
                                <th>Title</th>
                                <th>Description</th>
                                <th>Status</th>
                                <th>Created</th>
                                <th class="text-center" style="width: 5%">
                                    <i class="fa fa-navicon"></i>
                                </th>
                            </tr>
                        </thead>
                        <tbody class="small">
                            @foreach($data as $row)
                            <tr>
                                <td></td>
                                <td>{!! $row->title !!}</td>
                                <td>{!! $row->info_excerpt !!}</td>
                                <td>
                                    @if($row->is_active == true)
                                        Active
                                    @elseif($row->is_active == false)
                                        Not Active
                                    @endif
                                </td>
                                <td>{!! $row->created_at !!}</td>
                                <td class="text-center">
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-xs btn-default dropdown-toggle" data-toggle="dropdown">
                                            <span class="caret"></span>
                                            <span class="sr-only">Action</span>
                                        </button>
                                        <ul class="dropdown-menu dropdown-menu-right" role="menu">
                                            {{-- <li> --}}
                                                {{-- <a href="{!! route('slider.show', $row->id) !!}"><i class="fa fa-eye"></i>Detail</a> --}}
                                            {{-- </li> --}}
                                            <li>
                                                <a href="{!! route('info.edit', $row->id) !!}"><i class="fa fa-edit"></i>Edit</a>
                                            </li>
                                            <li>
                                                <a href="#" data-toggle="modal" data-target="#delete-{!!$row->id!!}"><i class="fa fa-trash"></i>Delete</a>
                                            </li>
                                        </ul>
                                    </div>
                                </td>
                            </tr>
                            <!-- Modal -->
                            <div class="modal fade modal-danger" id="delete-{!!$row->id!!}" tabindex="-1" role="dialog" aria-labelledby="delete">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        {!! Form::open([ 'route' => ['info.destroy', $row->id], 'method' => 'delete' ]) !!}
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                                    aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title" id="myModalLabel">Konfirmasi</h4>
                                        </div>
                                        <div class="modal-body">
                                            Apakah anda yakin akan menghapus Info <b>{!! $row->title !!}</b>?
                                        </div>
                                        <div class="modal-footer">
                                            <button type="submit" class="btn btn-outline">Hapus</button>
                                        </div>
                                        {!! Form::close() !!}
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script>
        $(document).ready(function(){
            var t = $("#grid-info").DataTable({
                "columnDefs": [ 
                    {
                        "targets": 0,
                        "searchable": false,
                        "orderable": false,
                        "className": "dt-body-center",
                    },
                    {"targets": 5, "orderable": false},
                ],
                "order": [[4, 'desc']],
            });

            t.on('order.dt search.dt', function() {
                t.column(0, {search: 'applied', order: 'applied'}).nodes().each(function(cell, i) {
                    cell.innerHTML = i+1;
                });
            }).draw();
        });
    </script>
@endsection