@extends('cms::layouts.master')

@section('content')
    {!! Form::open([ 'route' => 'job.store', 'class' => 'form-horizontal', 'id' => 'form_jobs', 'autocomplete' => 'off' ]) !!}
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Jobs</h3>
                        <div class="box-tools pull-right">
                            <a class="btn btn-sm btn-danger" href="{!! route('job.index') !!}">
                                <i class="fa fa-times"></i>
                            </a>
                        </div>
                    </div>
                    <div class="box-body pad">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="title" class="col-sm-2 control-label">Title <span class="required">*</span></label>
                                    <div class="col-sm-8">
                                        {!! Form::text('title', null, [ 'class' => 'form-control', 'id' => 'title', 'maxlength' => 100, 'required', 'autofocus']) !!}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="slug" class="col-sm-2 control-label">Slug</label>
                                    <div class="col-sm-8">
                                        {!! Form::text('slug', null, [ 'class' => 'form-control', 'id' => 'slug', 'readonly']) !!}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="description" class="col-sm-2 control-label">Description <span class="required">*</span></label>
                                    <div class="col-sm-8">
                                        {!! Form::textarea('description', null, [ 'class' => 'form-control', 'id' => 'description', 'maxlength' => 1000, 'required']) !!}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="skills" class="col-sm-2 control-label">Skills</label>
                                    <div class="col-sm-8">
                                        {!! Form::textarea('skills', null, [ 'class' => 'form-control', 'id' => 'skills', 'maxlength' => 200]) !!}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="location" class="col-sm-2 control-label">Location</label>
                                    <div class="col-sm-8">
                                        {!! Form::text('location', null, [ 'class' => 'form-control', 'id' => 'location']) !!}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="degree_level" class="col-sm-2 control-label">Degree Level</label>
                                    <div class="col-sm-8">
                                        {!! Form::text('degree_level', null, [ 'class' => 'form-control', 'id' => 'degree_level']) !!}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="experience" class="col-sm-2 control-label">Experience</label>
                                    <div class="col-sm-3">
                                        <div class="input-group">
                                            {!! Form::number('experience', null, [ 'class' => 'form-control', 'id' => 'experience', 'min' => 0]) !!}
                                            <span class="input-group-addon" id="basic-addon2">Tahun</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="company" class="col-sm-2 control-label">Company</label>
                                    <div class="col-sm-8">
                                        {!! Form::text('company', null, [ 'class' => 'form-control', 'id' => 'company']) !!}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="salary" class="col-sm-2 control-label">Salary</label>
                                    <div class="col-sm-3">
                                        {!! Form::text('salary', null, [ 'class' => 'form-control text-right number', 'id' => 'salary']) !!}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="expired" class="col-sm-2 control-label">Expired</label>
                                    <div class="col-sm-3">
                                        {!! Form::text('expired', null, [ 'class' => 'form-control datepicker', 'id' => 'expired']) !!}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="is_active" class="col-sm-2 control-label">Active? </label>
                                    <div class="col-sm-8">
                                        <div class="checkbox">
                                            <label>
                                                {!! Form::checkbox('is_active', true, true) !!} Yes
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        <button type="reset" class="btn btn-default">Cancel</button>
                        <button type="submit" class="btn btn-info pull-right">Save</button>
                    </div>
                </div>
            </div>
        </div>
    {!! Form::close() !!}
@endsection

@section('scripts')
    <script>
        $(document).ready(function() {
            $("#description").wysihtml5({
                toolbar: {
                    size: "xs",
                    image: false,
                    blockquote: false
                }
            });
            $("#skills").wysihtml5({
                toolbar: {
                    size: "xs",
                    image: false,
                    blockquote: false
                }
            });
            $('.datepicker').datepicker({
                format: 'dd-mm-yyyy',
                autoclose: true,
                todayHighlight: true,
                weekStart: 1,
                startDate: new Date(),
                language: 'id'
            });
            $("#title").keyup(function(e) {
                let title = $(this).val();
                if(title.length > 0) {
                    let slug = slugThis(title);
                    $("#slug").val(slug);
                } else {
                    $("#slug").val('');
                }
            });
            $(".number").number(true, 0);

            $("#form_jobs").validate({
                errorElement: 'span',
                errorClass: 'help-block help-block-error small',
                focusInvalid: true,
                ignore: ":hidden:not(textarea)",
                rules: {
                    title: { required: true },
                    description: { required: true },
                    experience: { digits: true },
                    salary: { digits: true }
                },
                messages: {
                    title: { required: "harus diisi." },
                    description: { required: "harus diisi." },
                    experience: { digits: "harus angka." },
                    salary: { digits: "harus angka." }
                },
                highlight: function(element) {
                    $(element)
                        .closest('.form-group').addClass('has-error');
                },
                unhighlight: function(element) {
                    $(element)
                        .closest('.form-group').removeClass('has-error');
                },
                success: function(label) {
                    label
                        .closest('.form-group').removeClass('has-error');
                },
            });

            function slugThis(text) {
                return text
                    .toLowerCase()
                    .replace(/[^\w ]+/g,'')
                    .replace(/ +/g,'-');
            }
        });
        $(function () {
            $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
                checkboxClass: 'icheckbox_minimal-blue',
                radioClass: 'iradio_minimal-blue'
            });
        });
    </script>
@endsection