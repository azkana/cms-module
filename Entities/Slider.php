<?php

namespace Modules\CMS\Entities;

use Illuminate\Database\Eloquent\Model;

class Slider extends Model
{
    protected $table = 'cms_sliders';
    protected $fillable = [
        'is_text',
        'title',
        'caption_1',
        'caption_2',
        'caption_3',
        'is_image',
        'slider_img',
        'is_button',
        'btn_title',
        'btn_url',
        'order',
        'is_active'
    ];
}
