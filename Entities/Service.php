<?php

namespace Modules\CMS\Entities;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    protected $table = 'cms_services';
    protected $fillable = [
        'title',
        'description',
        'image',
        'order',
        'is_active'
    ];
}
