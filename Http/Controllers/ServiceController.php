<?php

namespace Modules\CMS\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\CMS\Entities\Service;
use Illuminate\Support\Facades\Storage;

class ServiceController extends Controller
{
    protected $disk;

    public function __construct()
    {
        $this->disk = config('cms.disk');
    }
    
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $params['pageTitle']    = 'Services';
        $params['pageSubTitle'] = 'Index';
        $params['data']         = Service::all();
        return view('cms::services.index', $params);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $params['pageTitle']    = 'Services';
        $params['pageSubTitle'] = 'Create';
        return view('cms::services.create', $params);
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        try {
            $data['title']      = trim($request->title);
            $data['description']= trim($request->description);
            $data['order']      = $request->order;
            $data['is_active']  = $request->is_active == 1 ? 1 : 0;

            if($request->hasFile('image')) {
                $fileExt    = strtolower($request->file('image')->getClientOriginalExtension());
                $fileName   = 'service-' . md5(date('Y-m-d').time()) . '.' . $fileExt;
                $data['image']   = $fileName;
                Storage::disk($this->disk)->put(
                    'cms/services/' . $fileName,
                    fopen($request->file('image'), 'r+'),
                    'public'
                );
            }

            Service::create($data);
            return redirect(route('service.index'))
				->with('success_message', trans('message.success_save'));
        } catch(\Exception $e) {
            return redirect(route('service.index'))
				->with('error_message', trans('message.error_save'));
        }
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show($id)
    {
        $params['pageTitle']    = 'Services';
        $params['pageSubTitle'] = 'Detail';
        $params['data']         = Service::findOrFail($id);
        return view('cms::services.show', $params);
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id)
    {
        $params['pageTitle']    = 'Services';
        $params['pageSubTitle'] = 'Edit';
        $params['data']         = Service::findOrFail($id);
        return view('cms::services.edit', $params);
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request, $id)
    {
        try {
            $service = Service::findOrFail($id);
            $data['title']      = trim($request->title);
            $data['description']= trim($request->description);
            $data['order']      = $request->order;
            $data['is_active']  = $request->is_active == 1 ? 1 : 0;

            if($request->hasFile('image')) {
                $fileExt    = strtolower($request->file('image')->getClientOriginalExtension());
                $fileName   = 'service-' . md5(date('Y-m-d').time()) . '.' . $fileExt;
                $data['image']   = $fileName;
                Storage::disk($this->disk)->put(
                    'cms/services/' . $fileName,
                    fopen($request->file('image'), 'r+'),
                    'public'
                );

                if(!empty($service->image)) {
                    Storage::disk('public')->delete('services/' . $service->image);
                }
            }

            $service->update($data);
            return redirect(route('service.index'))
				->with('success_message', trans('message.success_edit'));
        } catch(\Exception $e) {
            return redirect(route('service.index'))
				->with('error_message', trans('message.error_edit'));
        }
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id)
    {
        try {
            $data = Service::findOrFail($id);
            if(!empty($data->image)) {
                Storage::disk($this->disk)->delete('cms/services/' . $data->image);
            }
            $data->delete();
            return redirect(route('service.index'))
				->with('success_message', trans('message.success_delete'));
        } catch (\Exception $e) {
            return redirect(route('service.index'))
				->with('error_message', trans('message.error_delete'));
        }
    }
}
