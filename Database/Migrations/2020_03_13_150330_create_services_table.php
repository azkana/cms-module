<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cms_services', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title', 100)->comment('Title');
            $table->text('description', 5000)->comment('Description');
            $table->string('image', 100)->comment('Image');
            $table->integer('order')->comment('Order');
            $table->boolean('is_active')->comment('Status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cms_services');
    }
}
