<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSlidersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cms_sliders', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('is_text')->nullable()->comment('Text');
            $table->string('title', 100)->nullable()->comment('Title Slider');
            $table->string('caption_1', 100)->nullable()->comment('Caption 1');
            $table->string('caption_2', 100)->nullable()->comment('Caption 2');
            $table->string('caption_3', 100)->nullable()->comment('Caption 3');
            $table->boolean('is_image')->nullable()->comment('Image');
            $table->string('slider_img', 100)->nullable()->comment('Slider Image');
            $table->boolean('is_button')->nullable()->comment('Button');
            $table->string('btn_title', 50)->nullable()->comment('Button Title');
            $table->string('btn_url', 200)->nullable()->comment('Button URL');
            $table->integer('order')->nullable()->comment('Slider Order');
            $table->boolean('is_active')->nullable()->comment('Status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cms_sliders');
    }
}
