@extends('cms::layouts.master')

@section('content')

    {!! Form::open([ 'route' => 'client.store', 'class' => 'form-horizontal', 'files' => true, 'autocomplete' => 'off' ]) !!}
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">{!! $pageTitle !!}</h3>
                        <div class="box-tools pull-right">
                            <a class="btn btn-sm btn-danger" href="{!! URL::previous() !!}">
                                <i class="fa fa-times"></i>
                            </a>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="company_name" class="col-sm-4 control-label">Nama Perusahaan <span class="required">*</span></label>
                                    <div class="col-sm-8">
                                        {!! Form::text('company_name', null, [ 'class' => 'form-control', 'maxlength' => 100, 'required', 'autofocus']) !!}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="alias_name" class="col-sm-4 control-label">Alias</label>
                                    <div class="col-sm-8">
                                        {!! Form::text('alias_name', null, [ 'class' => 'form-control', 'maxlength' => 100]) !!}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="category" class="col-sm-4 control-label">Kategori</label>
                                    <div class="col-sm-8">
                                        {!! Form::text('category', null, [ 'class' => 'form-control', 'maxlength' => 100]) !!}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="website" class="col-sm-4 control-label">Website</label>
                                    <div class="col-sm-8">
                                        {!! Form::text('website', null, [ 'class' => 'form-control', 'maxlength' => 100]) !!}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="logo_img" class="col-sm-4 control-label">Logo</label>
                                    <div class="col-sm-8">
                                        {!! Form::file('logo_img', [ 'class' => '']) !!}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="is_active" class="col-sm-4 control-label">Active? </label>
                                    <div class="col-sm-6">
                                        <div class="checkbox">
                                            <label>
                                                {!! Form::checkbox('is_active', true, true) !!} Yes
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6"></div>
                        </div>
                    </div>
                    <div class="box-footer">
                        <button type="reset" class="btn btn-default">Cancel</button>
                        <button type="submit" class="btn btn-info pull-right">Save</button>
                    </div>
                </div>
            </div>
        </div>
    {!! Form::close() !!}
@endsection

@section('scripts')
    <script>
        $(document).ready(function(){
            var mapGroup = $('.map');
            mapGroup.addClass('hide');

            var hasMapBtn = $("#has_map");
            hasMapBtn.click(function() {
                if($(this).prop('checked') === true) {
                    mapGroup.removeClass('hide');
                } else if($(this).prop('checked') === false){
                    mapGroup.addClass('hide');
                }
            });
            
        });
        $(function () {
            $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
                checkboxClass: 'icheckbox_minimal-blue',
                radioClass: 'iradio_minimal-blue'
            });
        });
    </script>
@endsection