@extends('cms::frontend.layouts.app')

@section('content')

	<div class="jumbotron" style="background-image: url('{!! asset('modules/cms/frontend/img/bg.png') !!}');background-repeat: no-repeat;color: #ffffff">
		<div class="container">
			<h2 style="padding-top: 30px"><i>Info Terbaru</i></h2>
		</div>
	</div>

	<div class="container" style="padding-top:10px">
		<div class="col-md-12">
			<h3><b>{!! $data->title !!}</b></h3>
			<span>
				<small>
					<i class="glyphicon glyphicon-time"></i> {!! date( 'd M Y', strtotime($data->created_at)) !!}
				</small>
			</span>
			<br><br>
			{!! $data->description !!}
			@if(!empty($data->iframe_url))
				<iframe src="{!! $data->iframe_url !!}/pubhtml?widget=true&headers=false&chrome=false" height="{!! $data->iframe_height !!}" width="{!! $data->iframe_width !!}" frameborder="0"></iframe>
			@endif
			<br><br>
			<small>
				{!! $data->footnote !!}
			</small>
		</div>
    </div>

@endsection