<?php

namespace Modules\CMS\Entities;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuid as TraitsUuid;
use Illuminate\Support\Str;

class Jobs extends Model
{
    use TraitsUuid;

    protected $table = 'cms_jobs';

    public $incrementing    = false;
    
    protected $fillable = [
        'title', 'slug', 'desc', 'skills', 'location', 'degree_level', 'experience', 'company',
        'salary', 'expired', 'is_active'
    ];

    public function getJobExcerptAttribute()
    {
        return Str::words(strip_tags($this->desc), 20, ' ...');
    }
}
