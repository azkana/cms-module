<?php

namespace Modules\CMS\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\CMS\Entities\Slider;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Redis;

class SliderController extends Controller
{
    protected $disk;

    public function __construct()
    {
        $this->disk = config('cms.disk');
    }
    
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $params['pageTitle']    = 'Sliders';
        $params['pageSubTitle'] = 'Index';
        $params['data']         = Slider::all();
        return view('cms::sliders.index', $params);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $params['pageTitle']    = 'Sliders';
        $params['pageSubTitle'] = 'Create';
        return view('cms::sliders.create', $params);
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        try {
            $data['title']      = trim($request->title);
            $data['caption_1']  = trim($request->caption_1);
            $data['caption_2']  = trim($request->caption_2);
            $data['caption_3']  = trim($request->caption_3);
            $data['is_button']  = $request->is_button == 1 ? 1 : 0;
            $data['is_image']   = $request->is_image == 1 ? 1 : 0;
            $data['is_text']    = $request->is_text == 1 ? 1 : 0;
            $data['btn_title']  = trim($request->btn_title);
            $data['btn_url']    = trim($request->btn_url);
            $data['order']      = $request->order;
            $data['is_active']  = $request->is_active == 1 ? 1 : 0;
            
            if($request->hasFile('slider_img')) {
                $filename = 'slider-' . md5(date('Y-m-d').time());
                $path = 'cms/sliders/';
                $fullFilename = _upload_file($request->file('slider_img'), $filename, $path, $this->disk);
                $data['slider_img']   = $fullFilename;
            }
            Slider::create($data);
            return redirect(route('slider.index'))
				->with('success', trans('message.success_save'));
        } catch(\Exception $e) {
            return redirect(route('slider.index'))
				->with('error', trans('message.error_save'));
        }
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show($id)
    {
        $params['pageTitle']    = 'Sliders';
        $params['pageSubTitle'] = 'Detail';
        $params['data']         = Slider::findOrFail($id);
        return view('cms::sliders.show', $params);
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id)
    {
        $params['pageTitle']    = 'Sliders';
        $params['pageSubTitle'] = 'Edit';
        $params['data']         = Slider::findOrFail($id);
        return view('cms::sliders.edit', $params);
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request, $id)
    {
        try {
            $slider             = Slider::findOrFail($id);
            $data['title']      = trim($request->title);
            $data['caption_1']  = trim($request->caption_1);
            $data['caption_2']  = trim($request->caption_2);
            $data['caption_3']  = trim($request->caption_3);
            $data['is_button']  = $request->is_button == 1 ? 1 : 0;
            $data['is_image']   = $request->is_image == 1 ? 1 : 0;
            $data['is_text']    = $request->is_text == 1 ? 1 : 0;
            $data['btn_title']  = trim($request->btn_title);
            $data['btn_url']    = trim($request->btn_url);
            $data['order']      = $request->order;
            $data['is_active']  = $request->is_active == 1 ? 1 : 0;

            if($request->hasFile('slider_img')) {
                $filename = 'slider-' . md5(date('Y-m-d').time());
                $path = 'cms/sliders/';
                $fullFilename = _upload_file($request->file('slider_img'), $filename, $path, $this->disk);
                $data['slider_img']   = $fullFilename;
            }

            $slider->update($data);
            return redirect(route('slider.index'))
				->with('success', trans('message.success_edit'));
        } catch(\Exception $e) {
            return redirect(route('slider.index'))
				->with('error', trans('message.error_edit'));
        }
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id)
    {
        try {
            $data = Slider::findOrFail($id);
            if(!empty($data->slider_img)) {
                Storage::disk($this->disk)->delete('cms/sliders/' . $data->slider_img);
            }
            $data->delete();
            return redirect(route('slider.index'))
				->with('success', trans('message.success_delete'));
        } catch (\Exception $e) {
            return redirect(route('slider.index'))
				->with('error', trans('message.error_delete'));
        }
    }
}
