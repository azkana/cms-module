@extends('cms::frontend.layouts.app')

@section('content')

	<div class="jumbotron page-banner">
		<div class="container">
			<h2 style="padding-top: 30px"><i>Info Tes</i></h2>
		</div>
	</div>

	<div class="info-lists">
		<div class="container" style="padding-top:10px">
			<div class="row">
				<div class="col-md-8 col-md-offset-2">
                    <div class="form-group">
                        <div class="col-sm-4">
                            {!! Form::select('type', $type, null, ['class' => 'form-control input-lg', 'id' => 'type', 'placeholder' => 'Jenis Pencarian']) !!}
                        </div>

                        <div class="col-sm-8">
                            <div class="input-group input-group-lg">
                                <input type="text" class="form-control" id="input-text">
                                <span class="input-group-btn">
                                    <button type="button" class="btn btn-info btn-flat" id="btn-search">Cari</button>
                                </span>
                            </div>
                        </div>
                    </div>

				</div>
			</div>
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="alert alert-danger" style="display: none">
                        <h4><i class="icon fa fa-ban"></i> Error!</h4>
                        <span id="error-text"></span>
                    </div>

                    <table class="table table-bordered table-condensed" id="table-data" style="display: none; margin-left: 15px;; width:95%;">
                        <thead>
                            <tr>
                                <th class="text-center" style="width: 20%">No. Member</th>
                                <th class="text-center" style="width: 30%">NIK</th>
                                <th class="text-center">Nama</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="text-center" id="member"></td>
                                <td class="text-center" id="nik"></td>
                                <td class="text-center" id="nama"></td>
                            </tr>
                            <tr>
                                <td colspan="3" class="text-center" style="font-size: 18px; padding-top:20px">
                                    Nomor Tes anda adalah <span id="nomor-test" style="font-size: 20px; font-weight: bold; color: red"></span>.<br>
                                    Nomor Tes harap dicatat dan dihafalkan.
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
		</div>
	</div>

@endsection

@section('js')
    <script type="text/javascript">
        $(document).ready(function() {
            var type = $('#type');
            var input = $('#input-text');
            var btnSrc = $('#btn-search');
            var errTxt = $('#error-text');
            var alert = $('.alert');
            var table = $('#table-data');
            var member = $('#member');
            var nik = $('#nik');
            var nama = $('#nama');
            var noTest = $('#nomor-test');

            btnSrc.on('click', function(e) {
                errTxt.html('');
                alert.hide();
                if(type.val() === '') {
                    errTxt.append('- Jenis Pencarian harus dipilih!<br>');
                    alert.show();
                }

                if(input.val() === '') {
                    errTxt.append('- Text pencarian harus diisi!');
                    alert.show();
                }

                if(type.val() !== '' && input.val() !== '') {
                    $.ajax({
                        url: "{{ route('member.test.info') }}",
                        type: "GET",
                        data: {
                            "type": type.val(),
                            "query": input.val()
                        },
                        success: function(res) {
                            if(res.code == 200) {
                                if(res.data !== null) {
                                    member.html(res.data.member);
                                    nik.html(res.data.nik);
                                    nama.html(res.data.name);
                                    noTest.html(res.data.test);
                                    table.show();
                                } else {
                                    table.hide();
                                    errTxt.append('- Data tidak ditemukan.');
                                    alert.show();
                                }
                            } else {
                                table.hide();
                            }
                        },
                    });
                }
            });
        });
    </script>
@endsection

@section('css')

	<style>
		.page-banner {
			background-image: linear-gradient(rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)), url('{!! asset('modules/cms/frontend/img/news-banner.jpg') !!}');
			background-repeat: no-repeat;
			background-size: cover;
			background-position: center;
			color: #ffffff;
		}

		.info-lists {
			margin-bottom:20px;
   			padding:20px 0px;
		}

		.info-lists .row {
			margin-top:20px;
		}

		.info-lists a {
			color: black;
		}

		.info-lists a:hover {
			color:black;
			text-decoration:none;
		}

		.info-lists .card {
			background-color: #FFFFFF;
			padding:20px;
			-webkit-border-radius: 4px;
			-moz-border-radius: 4px;
			border-radius:4px;
			box-shadow: 0 4px 5px 0 rgba(0,0,0,0.14), 0 1px 10px 0 rgba(0,0,0,0.12), 0 2px 4px -1px rgba(0,0,0,0.3);
			height: 300px;
			opacity: 0.65;
		}

		.info-lists .card:hover {
			box-shadow: 0 16px 24px 2px rgba(0,0,0,0.14), 0 6px 30px 5px rgba(0,0,0,0.12), 0 8px 10px -5px rgba(0,0,0,0.3);
			color:black;
		}
	</style>

@endsection
