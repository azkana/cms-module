<?php

namespace Modules\CMS\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\CMS\Entities\Video;
use Illuminate\Support\Facades\Storage;

class VideoController extends Controller
{
    protected $pageTitle;
    protected $redirectTo;
    protected $disk;

    public function __construct()
    {
        $this->pageTitle = 'Video';
        $this->redirectTo= route('video.index');
        $this->disk = config('cms.disk');
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $params['pageTitle']    = $this->pageTitle;
        $params['pageSubTitle'] = 'Index';
        $params['data']         = Video::where('is_active', 1)->get();
        return view('cms::video.index', $params);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $params['pageTitle']    = $this->pageTitle;
        $params['pageSubTitle'] = 'Create';
        return view('cms::video.create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        try {
            $data['title']      = trim($request->title);
            $data['order']      = $request->order;
            $data['is_active']  = $request->is_active == 1 ? 1 : 0;
            if($request->hasFile('video_file')) {
                ini_set('memory_limit','256M');
                $fileExt    = strtolower($request->file('video_file')->getClientOriginalExtension());
                $fileName   = 'video-' . md5(date('Y-m-d').time()) . '.' . $fileExt;
                $data['file_name']   = $fileName;
                $data['file_type']   = $fileExt;
                $data['file_size']   = $request->file('video_file')->getSize();
                Storage::disk($this->disk)->put(
                    'cms/videos/' . $fileName,
                    fopen($request->file('video_file'), 'r+'),
                    'public'
                );
            }
            Video::create($data);
            return redirect(route('video.index'))
				->with('success', trans('message.success_save'));
        } catch (\Exception $e) {
            return redirect(route('video.index'))
				->with('error', trans('message.error_save'));
        }
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('cms::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id)
    {
        $params['pageTitle']    = $this->pageTitle;
        $params['pageSubTitle'] = 'Edit';
        $params['data']         = Video::findOrFail($id);
        return view('cms::video.edit', $params);
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request, $id)
    {
        try {
            $video = Video::findOrFail($id);
            $data['title']      = trim($request->title);
            $data['order']      = $request->order;
            $data['is_active']  = $request->is_active == 1 ? 1 : 0;
           
            $video->update($data);
            return redirect(route('video.index'))
				->with('success', trans('message.success_edit'));
        } catch (\Exception $e) {
            return redirect(route('video.index'))
				->with('error', trans('message.error_edit'));
        }
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id)
    {
        try {
            $data = Video::findOrFail($id);
            if(!empty($data->file_name)) {
                Storage::disk($this->disk)->delete('cms/videos/' . $data->file_name);
            }
            $data->delete();
            return redirect(route('video.index'))
				->with('success', trans('message.success_delete'));
        } catch (\Exception $e) {
            return redirect(route('video.index'))
				->with('error', trans('message.error_delete'));
        }
    }
}
