@extends('cms::layouts.master')

@section('content')
    {!! Form::open([ 'route' => 'info.store', 'class' => 'form-horizontal', 'files' => false, 'autocomplete' => 'off' ]) !!}
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Info</h3>
                        <div class="box-tools pull-right">
                            <a class="btn btn-sm btn-danger" href="{!! route('info.index') !!}">
                                <i class="fa fa-times"></i>
                            </a>
                        </div>
                    </div>
                    <div class="box-body pad">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="title" class="col-sm-2 control-label">Title <span class="required">*</span></label>
                                    <div class="col-sm-8">
                                        {!! Form::text('title', null, [ 'class' => 'form-control', 'id' => 'title', 'maxlength' => 100, 'required', 'autofocus']) !!}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="slug" class="col-sm-2 control-label">Slug</label>
                                    <div class="col-sm-8">
                                        {!! Form::text('slug', null, [ 'class' => 'form-control', 'id' => 'slug', 'readonly']) !!}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="description" class="col-sm-2 control-label">Description <span class="required">*</span></label>
                                    <div class="col-sm-8">
                                        {!! Form::textarea('description', null, [ 'class' => 'form-control', 'id' => 'description', 'maxlength' => 1000, 'required']) !!}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="iframe_url" class="col-sm-2 control-label">Iframe URL</label>
                                    <div class="col-sm-8">
                                        {!! Form::text('iframe_url', null, [ 'class' => 'form-control', 'maxlength' => 300]) !!}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="iframe_width" class="col-sm-2 control-label">Width</label>
                                    <div class="col-sm-3">
                                        {!! Form::number('iframe_width', 800, [ 'class' => 'form-control']) !!}
                                    </div>
                                    <label for="iframe_height" class="col-sm-2 control-label">Height</label>
                                    <div class="col-sm-3">
                                        {!! Form::number('iframe_height', 550, [ 'class' => 'form-control']) !!}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="footnote" class="col-sm-2 control-label">Footnote</label>
                                    <div class="col-sm-8">
                                        {!! Form::textarea('footnote', null, [ 'class' => 'form-control', 'id' => 'footnote', 'maxlength' => 500]) !!}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="is_active" class="col-sm-2 control-label">Active? </label>
                                    <div class="col-sm-8">
                                        <div class="checkbox">
                                            <label>
                                                {!! Form::checkbox('is_active', true, true) !!} Yes
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        <button type="reset" class="btn btn-default">Cancel</button>
                        <button type="submit" class="btn btn-info pull-right">Save</button>
                    </div>
                </div>
            </div>
        </div>
    {!! Form::close() !!}
@endsection

@section('scripts')
    <script>
        $(document).ready(function() {
            $("#description").wysihtml5({
                toolbar: {
                    html:true
                }
            });
            $("#footnote").wysihtml5({
                toolbar: {
                    html:true
                }
            });
            $("#title").keyup(function(e) {
                let title = $(this).val();
                if(title.length > 0) {
                    let slug = slugThis(title);
                    $("#slug").val(slug);
                } else {
                    $("#slug").val('');
                }
            });

            function slugThis(text) {
                return text
                    .toLowerCase()
                    .replace(/[^\w ]+/g,'')
                    .replace(/ +/g,'-');
            }
        });
        $(function () {
            $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
                checkboxClass: 'icheckbox_minimal-blue',
                radioClass: 'iradio_minimal-blue'
            });
        });
    </script>
@endsection