<?php

namespace Modules\CMS\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\CMS\Entities\Jobs;
use Illuminate\Support\Str;

class JobController extends Controller
{
    protected $pageTitle;
    protected $redirectTo;

    public function __construct()
    {
        $this->pageTitle = 'Jobs';
        $this->redirectTo= route('job.index');
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $params['pageTitle']    = $this->pageTitle;
        $params['pageSubTitle'] = 'Index';
        $params['data']         = Jobs::orderBy('created_at', 'DESC')->get();
        return view('cms::jobs.index', $params);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $params['pageTitle']    = $this->pageTitle;
        $params['pageSubTitle'] = 'Create';
        return view('cms::jobs.create', $params);
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        try {
            $data['title']  = $request->title;
            $data['slug']   = $request->slug . '-' . Str::random(5);
            $data['desc']   = $request->description;
            $data['skills']     = $request->skills;
            $data['location']   = $request->location;
            $data['degree_level'] = $request->degree_level;
            $data['experience'] = $request->experience;
            $data['company']    = $request->company;
            $data['salary']     = !empty($request->salary) ? cleanNominal($request->salary) : null;
            $data['expired']    = !empty($request->expired) ? dateFormatYmd($request->expired) : null;
            $data['is_active']  = $request->is_active == null ? 0 : 1;

            Jobs::create($data);

            return redirect($this->redirectTo)
                ->with('success', trans('message.success_save'));
        } catch (\Exception $e) {
            return redirect($this->redirectTo)
                ->with('error', trans('message.error_save'));
        }
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('cms::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id)
    {
        $params['pageTitle']    = $this->pageTitle;
        $params['pageSubTitle'] = 'Edit';
        $params['data'] = Jobs::findOrFail($id);
        return view('cms::jobs.edit', $params);
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request, $id)
    {
        try {
            $job    = Jobs::findOrFail($id);
            $data['title']  = $request->title;
            $data['slug']   = $request->slug;
            $data['desc']   = $request->description;
            $data['skills']     = $request->skills;
            $data['location']   = $request->location;
            $data['degree_level'] = $request->degree_level;
            $data['experience'] = $request->experience;
            $data['company']    = $request->company;
            $data['salary']     = !empty($request->salary) ? cleanNominal($request->salary) : null;
            $data['expired']    = !empty($request->expired) ? dateFormatYmd($request->expired) : null;
            $data['is_active']  = $request->is_active == null ? 0 : 1;

            $job->update($data);

            return redirect($this->redirectTo)
                ->with('success', trans('message.success_edit'));
        } catch (\Exception $ex) {
            return redirect($this->redirectTo)
                ->with('error', trans('message.error_edit'));
        }
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id)
    {
        try {
            $job    = Jobs::findOrFail($id);
            $job->delete();

            return redirect($this->redirectTo)
                ->with('success', trans('message.success_delete'));
        } catch (\Exception $ex) {
            return redirect($this->redirectTo)
                ->with('error', trans('message.error_delete'));
        }
    }
}
