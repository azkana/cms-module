@extends('cms::layouts.master')

@section('content')

    {!! Form::open([ 'route' => ['service.update', $data->id], 'method' => 'patch', 'class' => 'form-horizontal', 'files' => true, 'autocomplete' => 'off' ]) !!}
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">{!! $pageTitle !!}</h3>
                        <div class="box-tools pull-right">
                            <a class="btn btn-sm btn-danger" href="{!! route('service.index') !!}">
                                <i class="fa fa-times"></i>
                            </a>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="title" class="col-sm-2 control-label">Title <span class="required">*</span></label>
                                    <div class="col-sm-8">
                                        {!! Form::text('title', $data->title, [ 'class' => 'form-control', 'maxlength' => 100, 'required', 'autofocus']) !!}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="description" class="col-sm-2 control-label">Description <span class="required">*</span></label>
                                    <div class="col-sm-8">
                                        {!! Form::textarea('description', $data->description, [ 'class' => 'form-control', 'id' => 'description', 'maxlength' => 5000, 'required']) !!}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="image" class="col-sm-2 control-label">Image <span class="required">*</span></label>
                                    <div class="col-sm-8">
                                        {!! Form::file('image', null, [ 'class' => 'form-control']) !!}
                                        @if(!empty($data->image))
                                        <br>
                                        <img src="{!! asset('storage/services/' . $data->image) !!}" style="max-width: 300px; border-radius: 10px" />
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="order" class="col-sm-2 control-label">Order</label>
                                    <div class="col-sm-3">
                                        {!! Form::number('order', $data->order, [ 'class' => 'form-control']) !!}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="is_active" class="col-sm-2 control-label">Active? </label>
                                    <div class="col-sm-6">
                                        <div class="checkbox">
                                            <label>
                                                {!! Form::checkbox('is_active', true, $data->is_active == true ? true : false) !!} Yes
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        <button type="reset" class="btn btn-default">Cancel</button>
                        <button type="submit" class="btn btn-info pull-right">Save</button>
                    </div>
                </div>
            </div>
        </div>
    {!! Form::close() !!}

@endsection

@section('scripts')
    <script>
        $(document).ready(function() {
            $("#description").wysihtml5();
        });
    </script>
@endsection