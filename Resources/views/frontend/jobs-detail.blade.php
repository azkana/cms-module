@extends('cms::frontend.layouts.app')

@section('content')

	<div class="jumbotron page-banner">
		<div class="container">
			<h2 style="padding-top: 30px"><i>Lowongan Kerja</i></h2>
		</div>
	</div>

	<div class="container" style="padding-top:10px">
		<div class="row">
			<div class="col-md-8 col-lg-offset-2">
				<a href="{!! route('cms.home') !!}">Beranda</a> >
				<a href="{!! route('cms.job') !!}">Lowongan Kerja</a> >
				{!! $data->title !!}
			</div>
		</div>
		<div class="row" style="margin-top: 20px">
		  	<div class="col-md-8 col-lg-offset-2">
			  	<div class="panel panel-default">
				  	<div class="panel-body">
						<h3>{!! $data->title !!}</h3>
						<span>
							@if(!empty($data->company))
							<i class="glyphicon glyphicon-briefcase"></i> {!! $data->company !!}
							- 
							@endif
							@if(!empty($data->location))
							<i class="glyphicon glyphicon-map-marker"></i> {!! $data->location !!}
							@endif
						</span>
						<hr>
						<div class="row">
							@if(!empty($data->experience))
							<div class="col-md-4 col-sm-4 col-xs-6">
								<small>Pengalaman Kerja:</small><br>
								{!! $data->experience !!} Tahun
							</div>
							@endif
							@if(!empty($data->degree_level))
							<div class="col-md-4 col-sm-4 col-xs-6">
								<small>Pendidikan:</small><br>
								{!! $data->degree_level !!}
							</div>
							@endif
							@if(!empty($data->salary))
							<div class="col-md-4 col-sm-4 col-xs-6">
								<small>Gaji:</small><br>
								@if(!empty($data->salary))
									IDR {!! numberFormat($data->salary) !!}
								@else
									dirahasiakan
								@endif
							</div>
							@endif
						</div>
						@if(!empty($data->experience) OR !empty($data->degree_level) OR !empty($data->salary))
						<hr>
						@endif
						<h4><i class="glyphicon glyphicon-list-alt"></i> Deskripsi Pekerjaan</h4>
						<span>{!! $data->desc !!}</span>
						@if(!empty($data->skills))
						<h4><i class="glyphicon glyphicon-tasks"></i> Persyaratan</h4>
						<span>{!! $data->skills !!}</span>
						@endif
						<hr>
						<div class="row">
							<div class="col-md-6 col-sm-6 col-xs-6">
								<span title="Location">
									<small>
										<small>Dibuat pada</small><br> {!! date( 'd F Y', strtotime($data->created_at)) !!}
									</small>
								</span>
							</div>
							<div class="col-md-6 col-sm-6 col-xs-6">
								<span title="Posted" class="pull-right">
									<small>
										@if(!empty($data->expired))
											<small>Ditutup pada</small><br> 
											{!! date( 'd F Y', strtotime($data->expired)) !!}
										@endif
									</small>
								</span>
							</div>
						</div>
						<hr>
						<!-- ShareThis BEGIN -->
						<div class="sharethis-inline-share-buttons"></div>
						<!-- ShareThis END -->
				  	</div>
			  	</div>
		  	</div>
		</div>
  	</div>

@endsection

@section('css')

	<style>
		.page-banner {
			background-image: linear-gradient(rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)), url('{!! asset('modules/cms/frontend/img/jobs-banner.jpg') !!}');
			background-repeat: no-repeat;
			background-size: cover;
			background-position: center;
			color: #ffffff;
		}
	</style>

@endsection