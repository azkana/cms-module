<li class="treeview @if($adminActiveMenu == 'cms') active @endif">
    <a href="#">
        <i class="fa fa-globe"></i>
        <span>Website</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu">
        <li class="@if($adminActiveSubMenu == 'sliders') active @endif"><a href="{!! route('slider.index') !!}"><i class="fa fa-circle-o"></i> Sliders</a></li>
        @can('cms-jobs-list')
        <li class="@if($adminActiveSubMenu == 'jobs') active @endif"><a href="{!! route('job.index') !!}"><i class="fa fa-circle-o"></i> Jobs</a></li>
        @endcan
        <li class="@if($adminActiveSubMenu == 'info') active @endif"><a href="{!! route('info.index') !!}"><i class="fa fa-circle-o"></i> Info</a></li>
        <li class="@if($adminActiveSubMenu == 'videos') active @endif"><a href="{!! route('video.index') !!}"><i class="fa fa-circle-o"></i> Video</a></li>
        <li class="@if($adminActiveSubMenu == 'about') active @endif"><a href="{!! route('about.index') !!}"><i class="fa fa-circle-o"></i> About Us</a></li>
        <li class="@if($adminActiveSubMenu == 'service') active @endif"><a href="{!! route('service.index') !!}"><i class="fa fa-circle-o"></i> Services</a></li>
        <li class="@if($adminActiveSubMenu == 'client') active @endif"><a href="{!! route('client.index') !!}"><i class="fa fa-circle-o"></i> Clients</a></li>
        <li class="@if($adminActiveSubMenu == 'contact') active @endif"><a href="{!! route('contact.index') !!}"><i class="fa fa-circle-o"></i> Contacts</a></li>
        <li class="@if($adminActiveSubMenu == 'setting') active @endif"><a href="{!! route('setting.index') !!}"><i class="fa fa-circle-o"></i> Settings</a></li>
    </ul>
</li>
