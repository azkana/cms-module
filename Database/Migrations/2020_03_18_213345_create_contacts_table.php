<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cms_contact', function (Blueprint $table) {
            $table->increments('id');
            $table->string('company_name', 100)->comment('Nama Perusahaan');
            $table->string('building_name', 100)->comment('Nama Gedung');
            $table->string('street_name', 100)->comment('Nama Jalan');
            $table->string('street_area', 100)->comment('Wilayah Jalan');
            $table->string('phone', 50)->comment('Nomor Telpon');
            $table->string('fax', 50)->comment('Nomor Fax');
            $table->string('email', 100)->comment('Email');
            $table->boolean('is_branch')->comment('Kantor Cabang');
            $table->boolean('has_map')->comment('Ada Map');
            $table->string('longitude', 30)->comment('Longitude');
            $table->string('latitude', 30)->comment('Latitude');
            $table->integer('zoom')->comment('Map Zoom');
            $table->boolean('is_active')->comment('Status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cms_contact');
    }
}
