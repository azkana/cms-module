@extends('cms::frontend.layouts.app')

@section('css')
	<link rel="stylesheet" href="https://cdn.plyr.io/3.6.4/plyr.css" />
	<style>
		@media only screen and (max-width: 640px) {
			.carousel {
				max-height: 500px;
				min-height: 200px;
				height: auto;
			}
			.carousel .item {
				max-height: 500px;
				min-height: 200px;
				height: auto;
			}
			.carousel-inner > .item > img {
				max-height: 500px;
				min-height: 200px;
				height: auto;
			}
			.carousel-caption > h1 {
				font-size: 18px;
			}
			.carousel-caption > p {
				font-size: 13px;
			}
			.imgpopup {
				max-height: 200px;
				margin-left: 10px;
				margin-top: 10px;
			}
			.txtpopup {
				max-width: 250px;
				padding-top: 50px; 
				margin-left: -5px;
				margin-top: -30px;
			}
			.btnpopup {
				width: 100%;
				margin-top: 20px
			}
			video {
			max-width: 100%;
			height: auto;
		}
		}
		@media only screen and (min-width: 768px) and (max-width: 1024px) {
			.carousel {
				max-height: 500px;
				min-height: 300px;
				height: auto;
			}
			.carousel .item {
				max-height: 500px;
				min-height: 300px;
				height: auto;
			}
			.carousel-inner > .item > img {
				max-height: 500px;
				min-height: 300px;
				height: auto;
			}
			.imgpopup {
				max-height: 600px;
				margin-left: -80px;
			}
			.txtpopup {
				margin-left: -30px;
				padding-top: 70px; 
				max-width: 470px;
			}
			.btnpopup {
				width: 60%;
				margin-top: -100px;
				margin-right: 100px;
				float: right;
			}
			video {
			max-width: 100%;
			height: auto;
		}
		}
		@media only screen and (min-width: 1024px) and (max-width: 1605px) {
			.carousel {
				max-height: 500px;
				min-height: 300px;
			}
			.carousel .item {
				max-height: 500px;
				min-height: 300px;
			}
			.carousel-inner > .item > img {
				max-height: 500px;
				min-height: 300px;
			}
			.imgpopup {
				max-height: 600px;
				margin-left: -120px;
			}
			.txtpopup {
				padding-top: 50px; 
				max-height: 450px;
			}
			.btnpopup {
				width: 60%;
				margin-top: -100px;
				margin-right: 120px;
				float: right;
			}
			video {
			max-width: 100%;
			max-height: auto;
		}
		}
		.about-us {
			font-family: Arial, Helvetica, sans-serif !important,
		}
	</style>
@endsection

@section('content')
	<div id="myCarousel" class="carousel slide" data-ride="carousel">
		{{-- Slider Indicators --}}
		@if(count($sliders) > 1)
		<ol class="carousel-indicators">
			@foreach ($sliders as $row)
				<li data-target="#myCarousel" data-slide-to="{!! $sequ++ !!}" class="{!! $seq++ == 0 ? 'active' : null !!}"></li>
			@endforeach
		</ol>
		@endif
		{{-- Slider Content --}}
		<div class="carousel-inner" role="listbox">
			@foreach ($sliders as $row)
			<div class="item {!! $slider_no++ == 0 ? 'active' : null !!}">
				@if($row->is_image == true)
					@if(!empty($row->slider_img))
						<img class="" src="{!! _get_file($row->slider_img, '/cms/sliders/', config('cms.disk')) !!}" alt="{!! $row->title !!}">
					@endif
				@endif
				<div class="container">
					<div class="carousel-caption">
						@if($row->is_text == true)
						<h1>{!! $row->title !!}</h1>
						<p>{!! $row->caption_1 !!}</p>
						<p>{!! $row->caption_2 !!}</p>
						<p>{!! $row->caption_3 !!}</p>
						@endif
						@if($row->is_button == true)
						<p><a class="btn btn-md btn-primary" href="{!! $row->btn_url !!}" role="button">{!! $row->btn_title !!}</a></p>
						@endif
					</div>
				</div>
			</div>
			@endforeach
		</div>
		@if(count($sliders) > 1)
		<a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
			<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
			<span class="sr-only">Previous</span>
		</a>
		<a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
			<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
			<span class="sr-only">Next</span>
		</a>
		@endif
	</div>

	{{-- Video  --}}
	@if(count($videos) > 0)
	<div class="col-md-12" style="margin-top: -20px;margin-bottom: 20px; text-align:center">
		@foreach ($videos as $video)
			<div class="col-md-12 col-xs-12 col-lg-4 col-lg-offset-4">
				<video autoplay alt="{!! $video->title !!}" id="player">
					<source src="{!! asset('/storage/videos/' . $video->file_name) !!}" type="video/mp4">
					{{-- <source src="{!! config('filesystems.disks.s3.url') . '/cms/videos/' . $video->file_name !!}" type="video/mp4"> --}}
					Your browser does not support the video tag.
				</video>
			</div>
		@endforeach
	</div>
	@endif

	<div class="container marketing" style="padding-top:10px">
		{{-- About --}}
		<div class="row featurette about-us">
			<div class="col-md-5">
				<img class="featurette-image img-responsive center-block" data-src="holder.js/500x500/auto" alt="500x500" src="{!! asset('storage/about/' . $about->image) !!}" data-holder-rendered="true">
			</div>
			<div class="col-md-7">
				<h3 class="featurette-heading" style="margin-top: 10px; font-size: 35px">{!! $about->title !!}</h3>
				<p class="lead text-justify" style="font-size: 18px; color: grey; line-height: 2em">
					{!! $about->description !!}
				</p>
			</div>
		</div>
		<hr class="featurette-divider">
		{{-- Services  --}}
		<div class="row">
			@foreach ($services as $row)
				<div class="col-lg-4">
					<img class="img-square" src="{!! asset('storage/services/' . $row->image) !!}" alt="{!! $row->title !!}" width="140" height="140">
					<h3>{!! $row->title !!}</h3>
					<p class="text-justify">
						{!! $row->description !!}
					</p>
				</div>
			@endforeach
		</div>
	</div>

	<div id="popoutAnnouncement" class="modal fade" tabindex="-1" role="dialog" data-backdrop="static">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content" style="background-color: #E4E4E2">
				<div class="row">
					<div class="col-md-3 col-xs-3">
						<img src="{!! config('filesystems.disks.s3.url') . '/cms/popup/dku-cs.png' !!}" class="imgpopup">
						{{-- <img src="{!! asset('storage/img/dku-cs.png') !!}" class="imgpopup"> --}}
					</div>
					<div class="col-md-9 col-xs-9">
						<img src="{!! config('filesystems.disks.s3.url') . '/cms/popup/text-popout.jpg' !!}" class="txtpopup">
						{{-- <img src="{!! asset('storage/img/text-popout.jpg') !!}" class="txtpopup"> --}}
					</div>
					<div class="col-md-12">
						<button type="button" class="btn btn-lg btn-success btnpopup" data-dismiss="modal">
							<strong>Saya Mengerti</strong>
						</button>
					</div>
				</div>
			</div>
		</div>
	</div>

@endsection

@section('js')
	<script src="https://cdn.plyr.io/3.6.4/plyr.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			$("#popoutAnnouncement").modal('toggle');
			$('.carousel').carousel({
				interval: 1000 * 8
			});
			const player = new Plyr('#player', {
				controls: ['play', 'progress', 'mute', 'volume', 'pip', 'fullscreen'],
				autopause: true,
				resetOnEnd: true
			});
		});
	</script>
@endsection