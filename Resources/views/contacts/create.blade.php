@extends('cms::layouts.master')

@section('content')

    {!! Form::open([ 'route' => 'contact.store', 'class' => 'form-horizontal', 'autocomplete' => 'off' ]) !!}
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">{!! $pageTitle !!}</h3>
                        <div class="box-tools pull-right">
                            <a class="btn btn-sm btn-danger" href="{!! route('contact.index') !!}">
                                <i class="fa fa-times"></i>
                            </a>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="company_name" class="col-sm-4 control-label">Perusahaan <span class="required">*</span></label>
                                    <div class="col-sm-8">
                                        {!! Form::text('company_name', null, [ 'class' => 'form-control', 'maxlength' => 100, 'required', 'autofocus']) !!}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="building_name" class="col-sm-4 control-label">Gedung <span class="required">*</span></label>
                                    <div class="col-sm-8">
                                        {!! Form::text('building_name', null, [ 'class' => 'form-control', 'maxlength' => 100]) !!}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="street_name" class="col-sm-4 control-label">Jalan</label>
                                    <div class="col-sm-8">
                                        {!! Form::text('street_name', null, [ 'class' => 'form-control', 'maxlength' => 100]) !!}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="street_area" class="col-sm-4 control-label">Wilayah</label>
                                    <div class="col-sm-8">
                                        {!! Form::text('street_area', null, [ 'class' => 'form-control', 'maxlength' => 100]) !!}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="phone" class="col-sm-4 control-label">No. Telp</label>
                                    <div class="col-sm-8">
                                        {!! Form::text('phone', null, [ 'class' => 'form-control', 'maxlength' => 50]) !!}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="fax" class="col-sm-4 control-label">Fax</label>
                                    <div class="col-sm-8">
                                        {!! Form::text('fax', null, [ 'class' => 'form-control', 'maxlength' => 50]) !!}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="email" class="col-sm-4 control-label">Email</label>
                                    <div class="col-sm-8">
                                        {!! Form::email('email', null, [ 'class' => 'form-control', 'maxlength' => 100]) !!}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="is_branch" class="col-sm-4 control-label">Cabang? </label>
                                    <div class="col-sm-6">
                                        <div class="checkbox">
                                            <label>
                                                {!! Form::checkbox('is_branch', true, false) !!} Yes
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="is_active" class="col-sm-4 control-label">Active? </label>
                                    <div class="col-sm-6">
                                        <div class="checkbox">
                                            <label>
                                                {!! Form::checkbox('is_active', true, true) !!} Yes
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="has_map" class="col-sm-4 control-label">Map? </label>
                                    <div class="col-sm-6">
                                        <div class="checkbox">
                                            <label>
                                                {!! Form::checkbox('has_map', true, false, ['id' => 'has_map']) !!} Yes
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group map">
                                    <label for="latitude" class="col-sm-4 control-label">Latitude</label>
                                    <div class="col-sm-6">
                                        {!! Form::text('latitude', null, [ 'class' => 'form-control', 'maxlength' => 30]) !!}
                                    </div>
                                </div>
                                <div class="form-group map">
                                    <label for="longitude" class="col-sm-4 control-label">Longitude</label>
                                    <div class="col-sm-6">
                                        {!! Form::text('longitude', null, [ 'class' => 'form-control', 'maxlength' => 30]) !!}
                                    </div>
                                </div>
                                <div class="form-group map">
                                    <label for="zoom" class="col-sm-4 control-label">Zoom</label>
                                    <div class="col-sm-4">
                                        {!! Form::select('zoom', $mapZoomLevel, 15, [ 'class' => 'form-control']) !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        <button type="reset" class="btn btn-default">Cancel</button>
                        <button type="submit" class="btn btn-info pull-right">Save</button>
                    </div>
                </div>
            </div>
        </div>
    {!! Form::close() !!}
@endsection

@section('scripts')
    <script>
        $(document).ready(function(){
            var mapGroup = $('.map');
            mapGroup.addClass('hide');

            var hasMapBtn = $("#has_map");
            hasMapBtn.click(function() {
                if($(this).prop('checked') === true) {
                    mapGroup.removeClass('hide');
                } else if($(this).prop('checked') === false){
                    mapGroup.addClass('hide');
                }
            });
            
        });
        $(function () {
            $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
                checkboxClass: 'icheckbox_minimal-blue',
                radioClass: 'iradio_minimal-blue'
            });
        });
    </script>
@endsection