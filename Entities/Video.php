<?php

namespace Modules\CMS\Entities;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuid as TraitsUuid;

class Video extends Model
{
    use TraitsUuid;

    protected $table = 'cms_video';

    public $incrementing    = false;

    protected $fillable = [
        'title', 'file_name', 'file_type', 'file_size', 'is_active', 'order'
    ];
}
